package fr.qcqteam.modular;

import fr.qcqteam.modular.command.CommandHandler;
import fr.qcqteam.modular.command.completer.CommandCompleter;
import fr.qcqteam.modular.database.DAOFactory;
import fr.qcqteam.modular.database.DAOType;
import fr.qcqteam.modular.module.IModule;
import fr.qcqteam.modular.module.ModuleDescription;
import fr.qcqteam.modular.module.ModuleManager;
import fr.qcqteam.modular.module.command.ModuleCommand;
import fr.qcqteam.modular.player.*;
import fr.qcqteam.modular.utils.Logger;
import fr.qcqteam.modular.utils.ModularConfiguration;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.Optional;

/**
 * Created by Qilat on 26/03/2018 for modularplugin.
 */
public class ModularCore {

    private static ModularCore instance;
    @Getter
    private final Plugin plugin;
    @Getter
    private final ModularConfiguration config;
    @Getter
    private FileConfiguration modularConfig;
    @Getter
    private ModuleManager moduleManager;
    @Getter
    private DAOFactory daoFactory;

    private AbstractPlayerManager<?, ?> dataManager;
    @Getter
    private IModule pluginModule;

    public ModularCore(Plugin plugin) {
        this(plugin, ModularConfiguration.DEFAULT);
    }

    public ModularCore(Plugin plugin, ModularConfiguration configuration) {
        this.plugin = plugin;
        this.config = configuration;
    }

    public static ModularCore get() {
        return instance;
    }

    protected void registerDataManager() {
        this.registerDataManager(new DefaultPlayerManager());
    }

    protected void registerDataManager(AbstractPlayerManager<?, ?> manager) {
        this.dataManager = manager;

        Logger.info("Modular data connection initialization.");
        DAOType type;
        try {
            type = DAOType.valueOf(modularConfig.getString("storage_type"));
            Logger.info("Data connection type set to : " + type.name());
        } catch (IllegalArgumentException e) {
            type = DAOType.XML;
            Logger.info("An error occurs while reading modular configuration. Data connection set by default at : " + type.name());
        }
        this.daoFactory = new DAOFactory(type);
        manager.initParameter();
        manager.registerConnectListener(this.plugin);
    }

    public File getDataFolder() {
        return this.plugin.getDataFolder();
    }

    public void onLoad() {
        instance = this;
        Logger.LOGGER = this.plugin.getLogger();
        Logger.info("Logger initialized.");

        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.DATA)) {
            this.plugin.saveDefaultConfig();
            modularConfig = this.plugin.getConfig();
            Logger.info("Modular config loaded.");
        }

        Logger.info("Creating modules manager instance...");
        this.moduleManager = new ModuleManager();
    }

    public void onEnable() {
        if (this.plugin.getDescription().getMain().equals("fr.modular.plugin.ModularPlugin"))
            registerDataManager();

        pluginModule = new IModule() {
            @Override
            public ModuleDescription getDescriptionFile() {
                return new ModuleDescription(ModularCore.this);
            }
        };
        CommandCompleter.registerDefaultCompleterArgs();
        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.MODULE))
            CommandHandler.registerCommand(ModuleCommand.class, this);
        Logger.info("Command system initialized.");
        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.MODULE)) {
            this.moduleManager.loadModules();
            Logger.info(this.moduleManager.getModules().size() + " modules loaded & enabled");
        }
    }

    public void onDisable() {

        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.DATA)) {
            getDataManager().ifPresent(data -> {
                Logger.info("Saving players data...");
                data.savePlayerInfos();
                Logger.info("Players data saved.");
            });
        }

        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.MODULE)) {
            Logger.info("Disabling modules...");
            this.moduleManager.disableModules();
            Logger.info("All modules disabled.");
        }

        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.COMMAND)) {
            CommandHandler.unregisterPluginCommands(this);
            Logger.info("Command system turned off.");
        }
        if (config.hasConfiguration(ModularConfiguration.ModularConfigurationType.DATA)) {
            this.daoFactory.closeConnection();
            Logger.info("Database connection closed.");
        }
    }

    public <T extends IModule> Optional<T> getModule(String key) {
        try {
            return (Optional<T>) getModuleManager().getModule(key);
        } catch (ClassCastException ex) {
            ex.printStackTrace();
            return Optional.empty();
        }
    }

    public Optional<AbstractPlayerManager<?, ?>> getDataManager() {
        return Optional.ofNullable(dataManager);
    }

    public void registerProperty(Class<? extends PlayerExtendedProperty<PlayerInfo>> propertyClass, IModule module) {
        PlayerPropertyManager.get().registerProperty(module.getDescriptionFile().getName(), PlayerPropertyManager.get().getPropertyTag(propertyClass), propertyClass);
    }
}
