package fr.qcqteam.modular.database;

import lombok.Getter;

import java.util.Map;

public abstract class DAO<T, R extends DAOParameter, C extends Connection> {

    @Getter
    C connection;

    public DAO(C connection) {
        this.connection = connection;
    }

    public abstract void load(T object, R param);

    public abstract void save(T object, R param);

    public abstract R getDefaultParameter();

}
