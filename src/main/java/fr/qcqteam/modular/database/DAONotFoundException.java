package fr.qcqteam.modular.database;

public class DAONotFoundException extends Exception {
    DAONotFoundException(Class clazz) {
        super("DAO class not found for this class : " + clazz.getCanonicalName());
    }
}
