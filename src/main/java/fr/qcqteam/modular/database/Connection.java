package fr.qcqteam.modular.database;

public interface Connection {

    void init();

    void close();

}
