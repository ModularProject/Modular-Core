package fr.qcqteam.modular.database.sql;

import fr.qcqteam.modular.database.Connection;
import lombok.Getter;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class SQLConnection implements Connection {

    private final String protocol, host, user, password, database;
    private final int port;

    @Getter
    private java.sql.Connection sqlConnection;

    public SQLConnection(String protocol, String host, String user, String password, String database, int port) {
        this.protocol = protocol;
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.port = port;
    }

    @Override
    public void init() {
        try {
            sqlConnection = DriverManager.getConnection(protocol + host + (port == -1 ? "" : ":" + port) + "/" + database, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            sqlConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
