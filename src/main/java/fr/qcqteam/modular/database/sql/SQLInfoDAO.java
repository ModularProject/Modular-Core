package fr.qcqteam.modular.database.sql;

import com.google.gson.internal.$Gson$Preconditions;
import fr.qcqteam.modular.database.DAOParameter;
import fr.qcqteam.modular.info.Info;
import fr.qcqteam.modular.info.property.IExtendedProperty;
import fr.qcqteam.modular.info.property.SavedItem;
import fr.qcqteam.modular.info.property.UnsavedProperty;
import fr.qcqteam.modular.utils.Logger;
import fr.qcqteam.modular.utils.Reflect;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Random;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class SQLInfoDAO extends SQLDAO<Info> {

    public SQLInfoDAO(SQLConnection connection) {
        super(connection);
    }

    @Override
    public void load(Info info, SQLDAOParameter params) {
        for (Map.Entry<String, ? extends IExtendedProperty> entryProperty : ((Info<? extends IExtendedProperty>) info).getPropertiesMap().entrySet()) {
            IExtendedProperty property = entryProperty.getValue();
            TableReference table = new TableReference(property.getPropertyTag());
            table.load(getConnection().getSqlConnection(), "_id", info.getId());
            if (!UnsavedProperty.class.isAssignableFrom(IExtendedProperty.class)
                    && !table.isEmpty()) {
                property.preLoad();
                if (ITableReference.class.isAssignableFrom(property.getClass())) {
                    ((ITableReference) property).fromReference(table);
                } else {
                    for (Field field : Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class)) {
                        if (field.isAnnotationPresent(SavedItem.class)) {
                            Serializable value = table.get(field.getName());
                            if (value != null) {
                                boolean isAccessible = field.isAccessible();
                                field.setAccessible(true);
                                try {
                                    if (ITableReference.class.isAssignableFrom(field.getType())) {
                                        TableReference reference = new TableReference(field.getType().getSimpleName());
                                        reference.load(getConnection().getSqlConnection(), "_id", value);
                                        Object obj = field.getType().newInstance();
                                        ((ITableReference)obj).fromReference(reference);
                                        field.set(property, obj);
                                    } else
                                        field.set(property, value);
                                } catch (IllegalAccessException | InstantiationException e) {
                                    e.printStackTrace();
                                }
                                field.setAccessible(isAccessible);
                            }
                        }
                    }
                }
                property.postLoad();
            } else {
                property.setDefault();
            }
        }
    }

    @Override
    public void save(Info info, SQLDAOParameter params) {
        for (Map.Entry<String, ? extends IExtendedProperty> entryProperty : ((Info<? extends IExtendedProperty>) info).getPropertiesMap().entrySet()) {
            IExtendedProperty property = entryProperty.getValue();
            TableReference table = new TableReference(property.getPropertyTag());
            table.load(this.getConnection().getSqlConnection(), "_id", info.getId());
            if (table.isEmpty()) {
                table.put("_id", info.getId());
            }
            if (!UnsavedProperty.class.isAssignableFrom(property.getClass())) {
                property.preSave();
                if (ITableReference.class.isAssignableFrom(property.getClass())) {
                    TableReference reference = ((ITableReference) property).toReference(property.getPropertyTag());
                    reference.put("_id", info.getId(), true);
                    reference.save(getConnection().getSqlConnection(), "_id");
                } else {
                    for (Field field : Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class)) {
                        Serializable backValue = table.get(field.getName());
                        if (field.isAnnotationPresent(SavedItem.class)) {
                            boolean isAccessible = field.isAccessible();
                            field.setAccessible(true);
                            try {
                                if (ITableReference.class.isAssignableFrom(field.getType())) {
                                    ITableReference doc = (ITableReference) field.get(property);
                                    Serializable id = backValue == null ? new Random().nextInt(25000) : backValue;
                                    table.put(field.getName(), id);
                                    TableReference reference = doc.toReference(field.getType().getSimpleName());
                                    reference.put("_id", id);
                                    reference.save(getConnection().getSqlConnection(), "_id");
                                } else {
                                    try {
                                        table.put(field.getName(), (Serializable) field.get(property));
                                    } catch (ClassCastException e) {
                                        Logger.severe("SavedItem must cover a Serializable type.", e);
                                    }
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            field.setAccessible(isAccessible);
                        }
                    }
                    table.save(getConnection().getSqlConnection(), "_id");
                }
                property.postSave();
            }
        }
    }
}
