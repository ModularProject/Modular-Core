package fr.qcqteam.modular.database.sql;

import fr.qcqteam.modular.database.Connection;
import fr.qcqteam.modular.database.DAO;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public abstract class SQLDAO<T> extends DAO<T, SQLDAOParameter, SQLConnection> {

    @SuppressWarnings("WeakerAccess")
    public SQLDAO(SQLConnection connection) {
        super(connection);
    }

    @Override
    public SQLDAOParameter getDefaultParameter() {
        return new SQLDAOParameter();
    }
}
