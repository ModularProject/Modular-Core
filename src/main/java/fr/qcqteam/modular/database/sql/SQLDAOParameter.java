package fr.qcqteam.modular.database.sql;

import fr.qcqteam.modular.database.DAOParameter;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class SQLDAOParameter extends DAOParameter {
    @Override
    protected void loadDefaults() {
        put(COLLECTION_KEY, "players");
    }
}
