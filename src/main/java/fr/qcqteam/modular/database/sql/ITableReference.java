package fr.qcqteam.modular.database.sql;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public interface ITableReference {

    TableReference toReference(String table);

    void fromReference(TableReference reference);

}
