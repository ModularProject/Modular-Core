package fr.qcqteam.modular.database.sql;

import fr.qcqteam.modular.utils.Logger;

import java.io.Serializable;
import java.sql.*;
import java.util.*;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class TableReference {

    private final String tableName;
    private final HashMap<String, Serializable> content = new HashMap<>();

    public TableReference(String tableName) {
        this.tableName = tableName;
    }

    public void put(String column, Serializable value, boolean replace) {
        if (content.containsKey(column) && !replace)
            return;
        content.remove(column);
        content.put(column, value);
    }

    public void put(String column, Serializable value) {
        this.put(column, value, true);
    }

    public void remove(String column) {
        content.remove(column);
    }

    public Serializable get(String column) {
        return content.get(column);
    }

    public <T extends Serializable> T get(String column, Class<T> tClass) {
        Serializable serializable = get(column);
        if (!tClass.isAssignableFrom(serializable.getClass())) {
            return null;
        }
        return (T)serializable;
    }

    public <T extends Serializable> T getOrDefault(String column, T defaultValue) {
        return (T) content.getOrDefault(column, defaultValue);
    }

    public boolean containsKey(String column) {
        return content.containsKey(column);
    }

    public boolean isEmpty() {
        return content.isEmpty();
    }

    void load(Connection connection, String key, Serializable value) {
        this.content.clear();
        this.createTable(connection, key, value);
        this.content.putAll(getLine(connection, key, value));
    }

    void save(Connection connection, String key) {
        Serializable value = get(key);
        this.createTable(connection, key, value);

        HashMap<String, Serializable> save = getLine(connection, key, value);
        List<String> columns = getColumns(connection);

        Logger.info(columns.toString());

        HashMap<String, Object> values = new HashMap<>();

        for (Map.Entry<String, Serializable> content : this.content.entrySet()) {
            if (!columns.contains(content.getKey())) {
                insertColumn(connection, content.getKey(), content.getValue().getClass(), null);
            }
            values.put(content.getKey(), content.getValue());
        }

        if (save.isEmpty()) {
            insert(connection, key, value, values);
        } else {
            set(connection, key, value, values);
        }
    }

    private void createTable(Connection connection, String key, Serializable value) {
        Map<String, Class<?>> struct = new HashMap<>();
        struct.put(key, value.getClass());
        this.createTable(connection, key, struct);
    }

    private void createTable(Connection connection, String key, Map<String, Class<?>> value){
        try {
            StringBuilder valuesBuilder = new StringBuilder("id INT AUTO_INCREMENT, ");

            for (Map.Entry<String, Class<?>> entry : value.entrySet()){
                valuesBuilder.append(entry.getKey());
                if (entry.getValue() == Integer.class || entry.getValue() == Double.class || entry.getValue() == Float.class){
                    valuesBuilder.append(" ").append("int(11)");
                } else {
                    valuesBuilder.append(" ").append("varchar(255)");
                }
                if (entry.getKey().equals(key))
                    valuesBuilder.append(" UNIQUE");
                valuesBuilder.append(", ");
            }
            valuesBuilder.append("PRIMARY KEY (id), UNIQUE (").append(key).append(")");

            PreparedStatement statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + tableName + " (" + valuesBuilder.toString() + ") CHARSET=latin1");
            statement.executeQuery();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void insertColumn(Connection connection, String key, Class<?> type, String addition) {
        try {
            String typeString;
            if (type == Integer.class || type == Double.class || type == Float.class){
                typeString = "int(11)";
            } else {
                typeString = "varchar(255)";
            }
            PreparedStatement statement = connection.prepareStatement("ALTER TABLE " + tableName + " ADD " + key + " " + typeString + (addition == null || addition.equals("") ? "" : " " + addition));
            statement.executeQuery().close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean exist(Connection connection, String key, Object obj) {
        try {
            final PreparedStatement ps = connection.prepareStatement("SELECT " + key + " FROM " + tableName + " WHERE " + key + "=?");
            ps.setString(1, obj.toString());
            boolean bool = ps.executeQuery().next();
            ps.close();
            return bool;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean insert(Connection connection, String keyColumnName, Object keyObj, Map<String, Object> value) {
        try {
            StringBuilder keysBuilder = new StringBuilder();
            StringBuilder valuesBuilder = new StringBuilder();
            for (Map.Entry<String, Object> entry : value.entrySet()) {
                if (entry.getKey().equals(""))
                    break;
                keysBuilder.append(entry.getKey()).append(",");
                valuesBuilder.append("?,");
            }
            String keys = keysBuilder.toString();
            String values = valuesBuilder.toString();
            if (keys.endsWith(","))
                keys = keys.substring(0, keys.toCharArray().length - 1);
            if (values.endsWith(","))
                values = values.substring(0, values.toCharArray().length - 1);
            if (!exist(connection, keyColumnName, keyObj)) {
                PreparedStatement q = connection.prepareStatement("INSERT INTO " + tableName + "(" + keys + ") VALUES " + "(" + values + ")");
                int i = 1;
                //Passage par du setObject() pour cause d'erreurs trop fréquentes.
                for (Object key : value.values()) {
                    if (key instanceof Integer) {
                        q.setInt(i, (Integer) key);
                    } else if (key instanceof Boolean) {
                        q.setBoolean(i, (Boolean) key);
                    } else {
                        q.setString(i, key.toString());
                    }
                    i++;
                }
                q.execute();
                q.close();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void set(Connection connection, String keyColumnName, Object key, Map<String, Object> value) {
        try {
            for (Map.Entry<String, Object> entry : value.entrySet()) {
                if (exist(connection, keyColumnName, key)) {
                    PreparedStatement q = connection.prepareStatement("UPDATE " + tableName + " SET " + entry.getKey() + "=?" + " WHERE " + keyColumnName + "=?");
                    if (entry.getValue() instanceof Integer) {
                        q.setInt(1, (Integer) entry.getValue());
                    } else if (entry.getValue() instanceof Boolean) {
                        q.setBoolean(1, (Boolean) entry.getValue());
                    } else {
                        q.setString(1, entry.getValue().toString());
                    }
                    if (key instanceof Integer) {
                        q.setInt(2, (Integer) key);
                    } else if (key instanceof Boolean) {
                        q.setBoolean(2, (Boolean) key);
                    } else {
                        q.setString(2, key.toString());
                    }
                    q.executeUpdate();
                    q.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void delete(Connection connection, String keyColumnName, Object key) {
        try {
            if (exist(connection, keyColumnName, key)) {
                PreparedStatement q = connection.prepareStatement("DELETE FROM " + tableName + " WHERE " + keyColumnName + "=?");
                q.setString(1, key.toString());
                q.execute();
                q.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, Serializable> getLine(Connection connection, String key, Object obj) {
        try {
            HashMap<String, Serializable> line = new HashMap<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE " + key + "=?");
            statement.setString(1, obj.toString());

            ResultSet result = statement.executeQuery();
            ResultSetMetaData metaData = result.getMetaData();
            if (result.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    line.put(metaData.getColumnName(i), (Serializable)result.getObject(i));
                }
            }
            statement.close();
            return line;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    private List<String> getColumns(Connection connection) {
        try {
            ArrayList<String> line = new ArrayList<>();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tableName);

            ResultSet result = statement.executeQuery();
            ResultSetMetaData metaData = result.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                line.add(metaData.getColumnName(i));
            }
            statement.close();
            return line;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private String getString(Connection connection, String key, Object obj, String valueKey) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT " + valueKey + " FROM " + tableName + " WHERE " + key + "=?");
            statement.setString(1, obj.toString());
            String s = "";
            ResultSet result = statement.executeQuery();
            if (result.next()){
                s = result.getString(valueKey);
            }
            statement.close();
            return s;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    private int getInt(Connection connection, String key, Object obj, String valueKey) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT " + valueKey + " FROM " + tableName + " WHERE " + key + "=?");
            statement.setString(1, obj.toString());
            int i = 0;
            ResultSet result = statement.executeQuery();
            if (result.next()){
                i = result.getInt(valueKey);
            }
            statement.close();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
