package fr.qcqteam.modular.database;

public enum DAOType {
    REDIS,
    SQL,
    MONGO,
    XML
}
