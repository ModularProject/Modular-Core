package fr.qcqteam.modular.database;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.database.mongo.MongoConnection;
import fr.qcqteam.modular.database.mongo.MongoInfoDAO;
import fr.qcqteam.modular.database.sql.SQLConnection;
import fr.qcqteam.modular.database.sql.SQLInfoDAO;
import fr.qcqteam.modular.info.Info;
import lombok.Getter;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class DAOFactory {

    private static final HashMap<DAOType, HashMap<Class<?>, Class<? extends DAO>>> map = new HashMap<>();

    static {
        for (DAOType type : DAOType.values()) {
            map.put(type, new HashMap<>());
        }

        addDAO(DAOType.MONGO, Info.class, MongoInfoDAO.class);
        addDAO(DAOType.SQL, Info.class, SQLInfoDAO.class);
    }

    @Getter
    private Connection connection;

    private DAOType daoType;

    public DAOFactory(DAOType daoType) {
        this.daoType = daoType;
        switch (this.daoType) {
            case MONGO:
                this.connection = new MongoConnection(
                        ModularCore.get().getModularConfig().getString("mongo_host", "localhost"),
                        ModularCore.get().getModularConfig().getInt("mongo_port", 27017),
                        ModularCore.get().getModularConfig().getString("mongo_user", null),
                        ModularCore.get().getModularConfig().getString("mongo_pass", null)
                );
                break;
            case SQL:
                try {
                    Class.forName("org.mariadb.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                this.connection = new SQLConnection(
                        ModularCore.get().getModularConfig().getString("sql_protocol", "jdbc:mysql://"),
                        ModularCore.get().getModularConfig().getString("sql_host", "localhost"),
                        ModularCore.get().getModularConfig().getString("sql_user", null),
                        ModularCore.get().getModularConfig().getString("sql_pass", null),
                        ModularCore.get().getModularConfig().getString("sql_database", "modular"),
                        ModularCore.get().getModularConfig().getInt("sql_port", -1)
                );
                break;
            case REDIS:
            case XML:
            default:
        }
        if (this.connection != null)
            this.connection.init();
    }

    @SuppressWarnings("WeakerAccess")
    public static <T, R extends DAOParameter, C extends Connection> void addDAO(DAOType type, Class<T> clazz, Class<? extends DAO<T, R, C>> daoClazz) {
        map.get(type).put(clazz, daoClazz);
    }

    public void closeConnection() {
        if (connection != null)
            connection.close();
    }

    @SuppressWarnings("unchecked")
    public <T, R extends DAOParameter, C extends Connection, D extends DAO<T, R, C>> D getDAO(Class<T> clazz) throws DAONotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        HashMap<Class<?>, Class<? extends DAO>> map = DAOFactory.map.get(this.daoType);
        Class daoClazz = map.get(clazz);
        if (daoClazz == null)
            throw new DAONotFoundException(clazz);
        return (D) daoClazz.getConstructor(connection.getClass()).newInstance(this.connection);
    }

}
