package fr.qcqteam.modular.database;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public abstract class DAOParameter {

    public static final String DB_KEY = "database";
    public static final String COLLECTION_KEY = "collection";

    protected final Map<String, String> defaultMap = new HashMap<>();

    public DAOParameter() {
        this.loadDefaults();
    }

    protected abstract void loadDefaults();

    public DAOParameter put(String key, String value, boolean replace) {
        if (defaultMap.containsKey(key) && !replace) {
            return this;
        }
        defaultMap.remove(key);
        defaultMap.put(key, value);
        return this;
    }

    public DAOParameter put(String key, String value) {
        return put(key, value, true);
    }

    public DAOParameter remove(String key) {
        defaultMap.remove(key);
        return this;
    }

    public String get(String key) {
        return defaultMap.get(key);
    }

}
