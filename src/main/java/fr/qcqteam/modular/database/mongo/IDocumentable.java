package fr.qcqteam.modular.database.mongo;

import org.bson.Document;

public interface IDocumentable {

    Document toDocument();

    void fromDocument(Document document);
}
