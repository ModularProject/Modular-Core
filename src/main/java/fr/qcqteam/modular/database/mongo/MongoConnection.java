package fr.qcqteam.modular.database.mongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import fr.qcqteam.modular.database.Connection;
import lombok.Getter;

public class MongoConnection implements Connection {

    @Getter
    private String host;
    @Getter
    private int port;
    @Getter
    private String user;
    private String password;

    @Getter
    private MongoClient client;

    public MongoConnection(String host, int port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
    }

    @Override
    public void init() {
        this.client = MongoClients.create("mongodb://" + ((user != null && password != null) ? (user + ":" + password + "@") : "") + host + ":" + port);
    }

    @Override
    public void close() {
        this.client.close();
    }
}
