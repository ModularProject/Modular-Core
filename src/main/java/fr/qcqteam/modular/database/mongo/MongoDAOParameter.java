package fr.qcqteam.modular.database.mongo;

import fr.qcqteam.modular.database.DAOParameter;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class MongoDAOParameter extends DAOParameter {
    @Override
    protected void loadDefaults() {
        this.put(DB_KEY, "modular");
        this.put(COLLECTION_KEY, "playerinfos");
    }
}
