package fr.qcqteam.modular.database.mongo;

import fr.qcqteam.modular.database.Connection;
import fr.qcqteam.modular.database.DAO;
import fr.qcqteam.modular.database.DAOParameter;

abstract class MongoDAO<T> extends DAO<T, MongoDAOParameter, MongoConnection> {

    @SuppressWarnings("WeakerAccess")
    public MongoDAO(MongoConnection connection) {
        super(connection);
    }

    @Override
    public MongoDAOParameter getDefaultParameter() {
        return new MongoDAOParameter();
    }
}
