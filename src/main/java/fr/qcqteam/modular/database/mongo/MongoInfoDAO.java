package fr.qcqteam.modular.database.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import fr.qcqteam.modular.database.DAOParameter;
import fr.qcqteam.modular.info.Info;
import fr.qcqteam.modular.info.property.IExtendedProperty;
import fr.qcqteam.modular.info.property.SavedItem;
import fr.qcqteam.modular.info.property.UnsavedProperty;
import fr.qcqteam.modular.utils.Reflect;
import org.bson.Document;

import java.lang.reflect.Field;
import java.util.Map;

public class MongoInfoDAO extends MongoDAO<Info> {

    public MongoInfoDAO(MongoConnection connection) {
        super(connection);
    }

    @Override
    public void load(Info info, MongoDAOParameter params) {
        MongoClient client = this.getConnection().getClient();
        MongoDatabase db = client.getDatabase(params.get(DAOParameter.DB_KEY));
        MongoCollection<Document> collection = db.getCollection(params.get(DAOParameter.COLLECTION_KEY));

        MongoCursor<Document> cursor = collection.find(new BasicDBObject("_id", info.getId())).cursor();

        if (cursor.hasNext()) {
            Document document = cursor.next();
            for (Map.Entry<String, ? extends IExtendedProperty> entryProperty : ((Info<? extends IExtendedProperty>) info).getPropertiesMap().entrySet()) {
                IExtendedProperty property = entryProperty.getValue();
                if (!UnsavedProperty.class.isAssignableFrom(IExtendedProperty.class)
                        && document.containsKey(entryProperty.getKey())) {
                    property.preLoad();
                    Document subDoc = document.get(entryProperty.getKey(), Document.class);
                    if (IDocumentable.class.isAssignableFrom(property.getClass())) {
                        ((IDocumentable) property).fromDocument(subDoc);
                    } else {
                        for (Field field : Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class)) {
                            if (field.isAnnotationPresent(SavedItem.class)) {
                                Object value = subDoc.get(field.getName());
                                if (value != null) {
                                    boolean isAccessible = field.isAccessible();
                                    field.setAccessible(true);
                                    try {
                                        if (IDocumentable.class.isAssignableFrom(field.getType())) {
                                            Object obj = field.getType().newInstance();
                                            ((IDocumentable)obj).fromDocument(subDoc.get(field.getName(), Document.class));
                                            field.set(property, obj);
                                        } else
                                            field.set(property, value);
                                    } catch (IllegalAccessException | InstantiationException e) {
                                        e.printStackTrace();
                                    }
                                    field.setAccessible(isAccessible);
                                }
                            }
                        }
                    }
                    property.postLoad();
                } else {
                    property.setDefault();
                }
            }
        }
    }

    @Override
    public void save(Info info, MongoDAOParameter params) {
        MongoClient client = this.getConnection().getClient();
        MongoDatabase db = client.getDatabase(params.get(DAOParameter.DB_KEY));
        MongoCollection<Document> collection = db.getCollection(params.get(DAOParameter.COLLECTION_KEY));

        Document document = new Document();
        MongoCursor<Document> cursor = collection.find(new BasicDBObject("_id", info.getId())).cursor();
        if (cursor.hasNext()) {
            document = cursor.next();
        } else
            document.append("_id", info.getId());

        for (Map.Entry<String, ? extends IExtendedProperty> entryProperty : ((Info<? extends IExtendedProperty>) info).getPropertiesMap().entrySet()) {
            IExtendedProperty property = entryProperty.getValue();
            if (!UnsavedProperty.class.isAssignableFrom(property.getClass())) {
                property.preSave();
                if (IDocumentable.class.isAssignableFrom(property.getClass())) {
                    document.remove(property.getPropertyTag());
                    document.append(entryProperty.getKey(), ((IDocumentable) property).toDocument());
                } else {
                    Document propertyDocument = new Document("_id", entryProperty.getKey());
                    for (Field field : Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class)) {
                        if (field.isAnnotationPresent(SavedItem.class)) {
                            boolean isAccessible = field.isAccessible();
                            field.setAccessible(true);
                            try {
                                if (IDocumentable.class.isAssignableFrom(field.getType())) {
                                    IDocumentable doc = (IDocumentable) field.get(property);
                                    propertyDocument.append(field.getName(), doc == null ? new Document() : doc.toDocument());
                                } else {
                                    propertyDocument.append(field.getName(), field.get(property));
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            field.setAccessible(isAccessible);
                        }
                    }
                    document.remove(property.getPropertyTag());
                    document.append(entryProperty.getKey(), propertyDocument);
                }
                property.postSave();
            }
        }
        collection.replaceOne(Filters.eq("_id", info.getId()), document, new UpdateOptions().upsert(true));
    }
}
