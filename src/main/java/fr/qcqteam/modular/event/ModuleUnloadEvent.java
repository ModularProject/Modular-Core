package fr.qcqteam.modular.event;


import fr.qcqteam.modular.module.IModule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by HoxiSword on 10/11/2019 for modular
 */
public class ModuleUnloadEvent extends Event {
    private static final HandlerList handlerList = new HandlerList();

    @Getter
    @Setter(AccessLevel.PRIVATE)
    private IModule module;

    public ModuleUnloadEvent(IModule module) {
        this.setModule(module);
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return handlerList;
    }
}
