package fr.qcqteam.modular.event;

import fr.qcqteam.modular.info.Info;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Qilat on 04/02/2018 for poudlardrp.
 * Call when a PlayerInfo construction is finish after init of the playerInfo's properties.
 */
public class InfoInitEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    @Getter
    @Setter(AccessLevel.PRIVATE)
    Info info;

    InfoInitEvent(Info info) {
        this.setInfo(info);
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    /**
     * Called before constructing a userinfo. Can be cancelled.
     * Properties must be add to a userinfo during this event.
     */
    public static class Pre extends InfoInitEvent {
        public Pre(Info info) {
            super(info);
        }
    }

    /**
     * Called after constructing a userinfo.
     */
    public static class Post extends InfoInitEvent {
        public Post(Info info) {
            super(info);
        }
    }

}
