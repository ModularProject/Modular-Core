package fr.qcqteam.modular.event;

import fr.qcqteam.modular.info.Info;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class InfoSaveEvent extends Event {
    private static final HandlerList handlerList = new HandlerList();

    @Getter
    @Setter(AccessLevel.PRIVATE)
    private Info info;

    InfoSaveEvent(Info info) {
        this.setInfo(info);
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    /**
     * Called before saving all data of a userinfo. Can be cancelled. If cancelled, not Post event will be throw.
     */
    public static class Pre extends InfoSaveEvent implements Cancellable {

        private boolean cancelled = false;

        public Pre(Info info) {
            super(info);
        }

        @Override
        public boolean isCancelled() {
            return cancelled;
        }

        @Override
        public void setCancelled(boolean cancel) {
            this.cancelled = cancel;
        }
    }
    /**
     * Called after saving all data of a userinfo.
     */
    public static class Post extends InfoSaveEvent {
        public Post(Info info) {
            super(info);
        }
    }
}
