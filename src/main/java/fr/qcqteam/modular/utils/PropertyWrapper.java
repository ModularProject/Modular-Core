package fr.qcqteam.modular.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.qcqteam.modular.info.property.IExtendedProperty;
import fr.qcqteam.modular.info.property.SavedItem;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PropertyWrapper {

    public static <P extends IExtendedProperty> String wrapToJSON(P property) {
        HashMap<String, String> propertyData = new HashMap<>();

        Iterable<Field> fields = Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class);
        fields.forEach(field -> {
            SavedItem desc = field.getAnnotation(SavedItem.class);
            if (desc != null) {
                try {
                    boolean oldAccessible = field.isAccessible();
                    field.setAccessible(true);
                    propertyData.put(field.getName(), new ObjectMapper().writeValueAsString(field.get(property)));
                    field.setAccessible(oldAccessible);
                } catch (IllegalAccessException | JsonProcessingException e) {
                    Logger.severe(ExceptionUtils.getStackTrace(e));
                }
            }
        });

        try {
            return new ObjectMapper().writeValueAsString(propertyData);
        } catch (JsonProcessingException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <P extends IExtendedProperty> void unwrapFromJSON(P property, String json) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
            };
            Map<String, String> propertyDataMap = mapper.readValue(json, typeRef);

            Iterable<Field> fields = Reflect.getFieldsUpTo(property.getClass(), IExtendedProperty.class);
            for (Field field : fields) {
                SavedItem desc = field.getAnnotation(SavedItem.class);
                if (desc != null) {
                    String valueStr = propertyDataMap.get(field.getName());
                    if (valueStr != null) {
                        Object fieldData;
                        if (Collection.class.isAssignableFrom(field.getType()) && desc.firstGenericClass() != Integer.class) {
                            fieldData = Reflect.getCollection(valueStr, (Class<? extends Collection>) field.getType(), desc.firstGenericClass());
                        } else if (Map.class.isAssignableFrom(field.getType()) && (desc.firstGenericClass() != Integer.class || desc.secondGenericClass() != Integer.class)) {
                            fieldData = Reflect.getMap(valueStr, (Class<? extends Map>) field.getType(), desc.firstGenericClass(), desc.secondGenericClass());
                        } else {
                            fieldData = mapper.readValue(valueStr, field.getType());
                        }

                        try {
                            boolean oldAccessible = field.isAccessible();
                            field.setAccessible(true);
                            if (fieldData != null)
                                field.set(property, fieldData);
                            field.setAccessible(oldAccessible);
                        } catch (IllegalAccessException e) {
                            Logger.severe(ExceptionUtils.getStackTrace(e));
                        }
                    }
                }
            }
        } catch (IOException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
    }

}
