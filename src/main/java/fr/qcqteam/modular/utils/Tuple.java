package fr.qcqteam.modular.utils;

public class Tuple<A, B> {

    private A t;
    private B t1;

    public Tuple(A t, B t1){
        this.t = t;
        this.t1 = t1;
    }


    public A getObjectA() {
        return t;
    }

    public B getObjectB() {
        return t1;
    }

    public void setObjectA(A t) {
        this.t = t;
    }

    public void setObjectB(B t1) {
        this.t1 = t1;
    }
}
