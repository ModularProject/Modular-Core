package fr.qcqteam.modular.utils;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.module.IModule;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListenerUtils {

    private static final HashMap<String, List<Listener>> modulesListeners = new HashMap<>();

    public static void registerListener(Listener listener, IModule module) {
        if (!modulesListeners.containsKey(module.getDescriptionFile().getName()))
            modulesListeners.put(module.getDescriptionFile().getName(), new ArrayList<>());
        modulesListeners.get(module.getDescriptionFile().getName()).add(listener);
        org.bukkit.Bukkit.getServer().getPluginManager().registerEvents(listener, ModularCore.get().getPlugin());
    }

    public static void unregisterListeners(IModule module){
        if (modulesListeners.containsKey(module.getDescriptionFile().getName()))
            for (Listener listener : modulesListeners.get(module.getDescriptionFile().getName()))
                HandlerList.unregisterAll(listener);
    }

}
