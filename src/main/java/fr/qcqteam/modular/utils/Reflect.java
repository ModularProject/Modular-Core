package fr.qcqteam.modular.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.collect.Lists;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Reflect {

    /**
     * Return value of a field by field name
     *
     * @param ownerObject Owner of the field
     * @param fieldName   Field Name
     * @return value of the field
     */
    public static Object getFieldValue(Object ownerObject, String fieldName) {
        try {
            Field field = ownerObject.getClass().getDeclaredField(fieldName);
            boolean oldAccessValue = field.isAccessible();
            field.setAccessible(true);
            Object fieldValue = field.get(ownerObject);
            field.setAccessible(oldAccessValue);
            return fieldValue;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    /**
     * Get all fields of a class (including fields of parent.). Ascending parents to exclusiveParent.
     *
     * @param startClass      target class
     * @param exclusiveParent parent class
     * @return all field declared or not of a class
     */
    public static Iterable<Field> getFieldsUpTo(Class<?> startClass, Class<?> exclusiveParent) {

        List<Field> currentClassFields = Lists.newArrayList(startClass.getDeclaredFields());
        Class<?> parentClass = startClass.getSuperclass();

        if (parentClass != null && (!(parentClass.equals(exclusiveParent)))) {
            List<Field> parentClassFields = (List<Field>) getFieldsUpTo(parentClass, exclusiveParent);
            currentClassFields.addAll(parentClassFields);
        }

        return currentClassFields;
    }

    /**
     * Create Collection Object from json string
     *
     * @param object          json string
     * @param collectionClass class inheriting from java.util.Collection
     * @param genericClass    generic type of collection
     * @param <Z>             generic type of collection
     * @return collection object
     */
    public static <Z> Collection<Z> getCollection(String object, Class<? extends Collection> collectionClass, Class genericClass) {
        Collection<Z> list = null;
        ObjectMapper mapper = new ObjectMapper();
        TypeFactory t = TypeFactory.defaultInstance();
        try {
            list = mapper.readValue(object, t.constructCollectionType(collectionClass, genericClass));
        } catch (IOException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
        return list;
    }

    /**
     * Create Map Object from json string
     *
     * @param object        json string
     * @param mapClass      class inheriting from java.util.Map
     * @param genericClass1 first generic type of map
     * @param genericClass2 second generic type of map
     * @param <Z>           first generic type of map
     * @param <T>           second generic type of map
     * @return map object
     */
    public static <Z, T> Map<Z, T> getMap(String object, Class<? extends Map> mapClass, Class genericClass1, Class genericClass2) {
        Map<Z, T> list = null;
        ObjectMapper mapper = new ObjectMapper();
        TypeFactory t = TypeFactory.defaultInstance();
        try {
            list = mapper.readValue(object, t.constructMapType(mapClass, genericClass1, genericClass2));
        } catch (IOException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
        return list;
    }

}
