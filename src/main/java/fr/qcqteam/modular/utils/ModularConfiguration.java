package fr.qcqteam.modular.utils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by HoxiSword on 28/07/2020 for modular
 */
public class ModularConfiguration {

    public static final ModularConfiguration DEFAULT = new ModularConfiguration(ModularConfigurationType.MODULE, ModularConfigurationType.DATA, ModularConfigurationType.COMMAND);

    private final ArrayList<ModularConfigurationType> types = new ArrayList<>();

    public ModularConfiguration(ModularConfigurationType... types) {
        this.types.addAll(Arrays.asList(types));
    }

    public boolean hasConfiguration(ModularConfigurationType type) {
        return types.contains(type);
    }

    public enum ModularConfigurationType {
        MODULE,
        DATA,
        COMMAND
    }

}
