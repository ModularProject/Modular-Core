package fr.qcqteam.modular.utils;

import java.util.logging.Level;

/**
 * Created by Theophile on 27/07/2018 for webapi.
 */
public class Logger {

    public static java.util.logging.Logger LOGGER;

    /**
     * Print string in console with Level.INFO value
     *
     * @param input string to print
     * @see java.util.logging.Logger
     */
    public static void info(String input) {
        LOGGER.info(input);
    }

    /**
     * Print string in console with Level.WARNING value
     *
     * @param input string to print
     * @see java.util.logging.Logger
     */
    public static void warning(String input) {
        LOGGER.log(Level.WARNING, input);
    }

    public static void warning(String input, Throwable throwable) {
        LOGGER.log(Level.WARNING, input, throwable);
    }

    /**
     * Print string in console with Level.SEVERE value
     *
     * @param input string to print
     * @see java.util.logging.Logger
     */
    public static void severe(String input) {
        LOGGER.log(Level.SEVERE, input);
    }

    public static void severe(String input, Throwable throwable){
        LOGGER.log(Level.SEVERE, input, throwable);
    }

}
