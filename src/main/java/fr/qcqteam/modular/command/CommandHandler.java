package fr.qcqteam.modular.command;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.command.utils.ICommand;
import fr.qcqteam.modular.module.IModule;
import fr.qcqteam.modular.utils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <b>Gestionnaire de commandes</b>
 *
 * @author HoxiSword
 * @version 1.0.0
 */
public class CommandHandler {

    public static final HashMap<String, LinkedHashMap<String, CommandSpec>> commands = new HashMap<>();

    private static LinkedHashMap<String, CommandSpec> getModuleCommands(IModule module){
        return commands.get(module.getDescriptionFile().getName());
    }

    /**
     * Méthode statique d'enregistrement de commande
     *
     * @param commandClass Class annotée avec TCommand
     * @param plugin       ModularCore instance
     * @see ICommand
     */
    public static void registerCommand(Class commandClass, ModularCore plugin) {
        registerCommand(commandClass, plugin.getPluginModule());
    }


    public static void registerCommand(Class commandClass, IModule module) {
        if (!commands.containsKey(module.getDescriptionFile().getName()))
            commands.put(module.getDescriptionFile().getName(), new LinkedHashMap<>());
        try {
            Map<String, CommandSpec> moduleCommands = getModuleCommands(module);
            for (CommandSpec spec : moduleCommands.values()) {
                if (spec.getCommandClass().equals(commandClass)) {
                    enableCommand(commandClass, module);
                    return;
                }
            }
            CommandSpec spec = new CommandSpec(commandClass);
            registerBukkitCommand(spec, module);
            moduleCommands.put(spec.getCommand().commandName(), spec);
        } catch (NoSuchFieldException | IllegalArgumentException e) {
            Logger.severe("Impossible d'enregistrer la commande " + commandClass.getName() + " : " + e.getMessage());
        } catch (IllegalAccessException e) {
            Logger.severe("Impossible d'accéder à la commande " + commandClass.getName() + " : " + e.getMessage());
        } catch (ReflectiveOperationException e) {
            Logger.severe("Impossible d'instancier la commande " + commandClass.getName() + " : " + e.getMessage());
        }
    }

    private static void registerBukkitCommand(CommandSpec spec, IModule module) throws NoSuchFieldException, IllegalAccessException {
        final Field commandFieldMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
        boolean oldAccessible = commandFieldMap.isAccessible();
        commandFieldMap.setAccessible(true);
        CommandMap map = (CommandMap) commandFieldMap.get(Bukkit.getServer());
        commandFieldMap.setAccessible(oldAccessible);
        CommandListener cmd = new CommandListener(spec.getCommand());
        map.register(module.getDescriptionFile().getName().toLowerCase().replace(" ", "_"), cmd);
        for (String alias : spec.getCommand().aliases())
            map.register(alias.toLowerCase(), module.getDescriptionFile().getName().toLowerCase().replace(" ", "_"), cmd);
    }

    public static void unregisterBukkitCommand(CommandSpec spec, IModule module) throws NoSuchFieldException, IllegalAccessException {
        final Field commandFieldMap = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
        boolean oldAccessible = commandFieldMap.isAccessible();
        commandFieldMap.setAccessible(true);
        SimpleCommandMap map = (SimpleCommandMap) commandFieldMap.get(Bukkit.getPluginManager());
        commandFieldMap.setAccessible(oldAccessible);
        Field knownCommand = SimpleCommandMap.class.getDeclaredField("knownCommands");
        oldAccessible = knownCommand.isAccessible();
        knownCommand.setAccessible(true);
        Map<String, Command> known = (Map<String, Command>) knownCommand.get(map);
        known.get(spec.getCommand().commandName().toLowerCase()).unregister(map);
        known.remove(spec.getCommand().commandName().toLowerCase());
        known.get(module.getDescriptionFile().getName().toLowerCase().replace(" ", "_") + ":" + spec.getCommand().commandName().toLowerCase()).unregister(map);
        known.remove(module.getDescriptionFile().getName().toLowerCase().replace(" ", "_") + ":" + spec.getCommand().commandName().toLowerCase());
        for (String alias : spec.getCommand().aliases()) {
            known.get(alias.toLowerCase()).unregister(map);
            known.remove(alias.toLowerCase());
            known.get(module.getDescriptionFile().getName().toLowerCase().replace(" ", "_") + ":" + alias.toLowerCase()).unregister(map);
            known.remove(module.getDescriptionFile().getName().toLowerCase().replace(" ", "_") + ":" + alias.toLowerCase());
        }
        knownCommand.setAccessible(oldAccessible);
    }

    public static void unregisterPluginCommands(ModularCore core){
        unregisterModuleCommands(core.getPluginModule());
    }

    public static void unregisterModuleCommands(IModule module){
        if (!commands.containsKey(module.getDescriptionFile().getName()))
            return;
        for (Map.Entry<String, CommandSpec> spec : new HashSet<>(getModuleCommands(module).entrySet())){
            try {
                unregisterBukkitCommand(spec.getValue(), module);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        commands.remove(module.getDescriptionFile().getName());
    }

    static LinkedHashMap<String, CommandSpec> getCommands() {
        LinkedHashMap<String, CommandSpec> allCommands = new LinkedHashMap<>();
        commands.forEach((s, moduleCommands) -> allCommands.putAll(moduleCommands));
        return allCommands;
    }

    public static void disableCommand(Class clazz, IModule module) {
        for (CommandSpec spec : getModuleCommands(module).values()){
            if (spec.getCommandClass().equals(clazz)){
                spec.setEnabled(false);
            }
        }
    }

    private static void enableCommand(Class clazz, IModule module){
        for (CommandSpec spec : getModuleCommands(module).values()){
            if (spec.getCommandClass().equals(clazz)){
                spec.setEnabled(true);
            }
        }
    }

}
