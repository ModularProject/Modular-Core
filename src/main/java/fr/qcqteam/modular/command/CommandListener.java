package fr.qcqteam.modular.command;

import fr.qcqteam.modular.command.completer.CommandCompleter;
import fr.qcqteam.modular.command.utils.CommandInfo;
import fr.qcqteam.modular.command.utils.CommandSenderType;
import fr.qcqteam.modular.command.utils.ICommand;
import fr.qcqteam.modular.command.utils.ICommandArg;
import fr.qcqteam.modular.utils.Logger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import javax.swing.text.html.Option;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CommandListener extends org.bukkit.command.Command {

    private static final String USAGE_MESSAGE = ChatColor.RED + "Usage : ";
    private static final String PERMISSION_DENIED = ChatColor.RED + "Vous n'avez pas la permission pour exéctuer cette commande.";
    private static final String COMMAND_SENDER_TYPE_INVALID = ChatColor.RED + "Vous ne pouvez pas exécuter cette commande depuis cette plateforme.";

    private final ICommand command;

    public CommandListener(ICommand command) {
        super(command.commandName());
        this.command = command;
    }

    /**
     * Méthode implémentée de Command
     * @see org.bukkit.command.Command
     *
     * Gestion des arguments de la commande locale
     */
    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if (commandSender instanceof ConsoleCommandSender && !Arrays.asList(this.command.commandSenderType()).contains(CommandSenderType.CONSOLE)) {
            commandSender.sendMessage(COMMAND_SENDER_TYPE_INVALID);
            return false;
        }

        CommandSpec spec = CommandHandler.getCommands().get(this.command.commandName());

        if (spec == null) {
            commandSender.sendMessage("§cCommande inconnue.");
            return false;
        }

        if (!spec.isEnabled()){
            commandSender.sendMessage("§cCommande désactivée.");
            return false;
        }

        if (commandSender instanceof Player && !spec.getCommand().permission().equals("") && !commandSender.hasPermission(spec.getCommand().permission()) && !commandSender.isOp() && !commandSender.hasPermission("*")) {
            commandSender.sendMessage(PERMISSION_DENIED);
            return false;
        }

        if (commandSender instanceof Player && !Arrays.asList(this.command.commandSenderType()).contains(CommandSenderType.PLAYER)) {
            commandSender.sendMessage(COMMAND_SENDER_TYPE_INVALID);
            return false;
        }

        strings = readQuoteArgs(strings);

        for (ICommandArg arg : spec.getArguments().keySet()) {
            if (arg.arguments().length == strings.length) {
                boolean bool = true;
                for (int i = 0; i < arg.arguments().length; i++) {
                    if ((!arg.arguments()[i].startsWith("%") && !arg.arguments()[i].endsWith("%")) && !arg.arguments()[i].equalsIgnoreCase(strings[i])) {
                        bool = false;
                        break;
                    }
                }

                if (commandSender instanceof ConsoleCommandSender && !Arrays.asList(arg.commandSenderType()).contains(CommandSenderType.CONSOLE)) {
                    commandSender.sendMessage(COMMAND_SENDER_TYPE_INVALID);
                    return false;
                }

                if (commandSender instanceof Player && !spec.getCommand().permission().equals("") && !commandSender.hasPermission(spec.getCommand().permission()) && !commandSender.isOp() && !commandSender.hasPermission("*")) {
                    commandSender.sendMessage(PERMISSION_DENIED);
                    return false;
                }

                if (commandSender instanceof Player && !Arrays.asList(arg.commandSenderType()).contains(CommandSenderType.PLAYER)) {
                    commandSender.sendMessage(COMMAND_SENDER_TYPE_INVALID);
                    return false;
                }

                if (bool) { // Les arguments correspondent
                    Optional<Class<? extends CommandInfo<? extends CommandSender>>> optionalClass = spec.getInfosType(arg);
                    if (!optionalClass.isPresent())
                        return false;
                    try {
                        spec.invokeArgument(arg, optionalClass.get().getConstructor(CommandSender.class, String[].class).newInstance(commandSender, strings));
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassCastException e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            }
        }
        commandSender.sendMessage(USAGE_MESSAGE + spec.getCommand().usage());
        return false;
    }

    /**
     * Méthode utilisée pour supprimer une commande du TAB.
     * @param target Envoyeur de la commande
     * @return la permission
     */
    @Override
    public boolean testPermissionSilent(CommandSender target) {
        return command.permission().equals("") || target.hasPermission(command.permission()) || target.isOp() || target.hasPermission("*") && !this.command.hidden();
    }

    @Override
    public boolean testPermission(CommandSender target) {
        return this.testPermissionSilent(target);
    }

    /**
     * Méthode implémentée de Command
     * @see org.bukkit.command.Command
     *
     * Gestion de la completion de la commande
     * @return la liste de String qui est proposée au joueur pour completer son choix
     */
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (!testPermissionSilent(sender))
            return new ArrayList<>();

        List<String> tabComplete = new ArrayList<>();

        CommandSpec spec = CommandHandler.getCommands().get(this.command.commandName());

        if (!spec.isEnabled())
            return tabComplete;

        args = this.readQuoteArgs(args);
        for (ICommandArg arg : spec.getArguments().keySet()) {
            if (arg.arguments().length < args.length || arg.hidden())
                continue;

            boolean bool = false;
            for (int i = 0; i < args.length - 1; i++) {
                if (!args[i].equalsIgnoreCase(arg.arguments()[i]) && !arg.arguments()[i].startsWith("%") && !arg.arguments()[i].endsWith("%")) {
                    bool = true;
                    break;
                }
            }

            if (bool)
                continue;


            String comp = arg.completerKeys().length >= args.length?arg.completerKeys()[args.length-1]: CommandCompleter.NO_COMPLETER;

            if (comp.equals(CommandCompleter.ARGUMENT_KEY)) {
                if (arg.arguments()[args.length - 1].startsWith(args[args.length - 1])) {
                    tabComplete.add(arg.arguments()[args.length - 1]);
                }
            } else {
                for (String completer : CommandCompleter.getCompleter(comp, sender)) {
                    if (completer.startsWith(args[args.length - 1])) {
                        tabComplete.add(completer);
                    }
                }
            }

        }

        return tabComplete;
    }

    private String[] readQuoteArgs(String[] strings) {
        ArrayList<String> newStrings = new ArrayList<>();

        for (int y = 0; y < strings.length; y++) {
            if (!strings[y].startsWith("\""))
                newStrings.add(strings[y]);
            else {
                StringBuilder builder = new StringBuilder(strings[y]);
                while (!strings[y].endsWith("\"")) {
                    y++;
                    if (strings.length <= y)
                        break;
                    builder.append(" ").append(strings[y]);
                }
                newStrings.add(builder.toString().replace("\"", ""));
            }
        }
        return newStrings.toArray(new String[0]);
    }
}
