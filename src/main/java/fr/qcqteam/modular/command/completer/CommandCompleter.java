package fr.qcqteam.modular.command.completer;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.command.CommandHandler;
import fr.qcqteam.modular.module.IModule;
import fr.qcqteam.modular.module.ModuleDescription;
import fr.qcqteam.modular.module.ModuleLoader;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <b>Enumération des types de complétion</b>
 *
 * @author HoxiSword
 * @version 1.0.0
 * @see CommandHandler
 */
public class CommandCompleter {

    public static final String ARGUMENT_KEY = "arg";
    public static final String NO_COMPLETER = "none";

    public static final String PLAYERS = "players";
    public static final String UNLOADED_MODULES = "unloaded_modules";
    public static final String LOADED_MODULES = "loaded_modules";

    private static HashMap<String, CompleterCallback> completer = new HashMap<>();

    public static void registerCompleterArg(String key, CompleterCallback callback){
        completer.put(key, callback);
    }

    public static List<String> getCompleter(String key, CommandSender sender){
        return completer.getOrDefault(key, s -> new ArrayList<>()).getResult(sender);
    }

    public static void registerDefaultCompleterArgs() {
        completer.put(PLAYERS, sender -> Bukkit.getOnlinePlayers().stream()
                .map(HumanEntity::getName)
                .collect(Collectors.toList()));

        completer.put(UNLOADED_MODULES, sender -> {
            HashMap<File, ModuleDescription> map = ModuleLoader.getAllModuleDescriptionFiles();
            return map.values().stream()
                    .filter(desc -> (desc != null && !ModularCore.get().getModuleManager().getModules().containsKey(desc.getName())))
                    .map(ModuleDescription::getName)
                    .collect(Collectors.toList());
        });

        completer.put(LOADED_MODULES, sender -> {
            List<String> returner = new ArrayList<>();
            for (IModule module : ModularCore.get().getModuleManager().getModules().values()) {
                if (module != null)
                    returner.add(module.getDescriptionFile().getName());
            }
            return returner;
        });
    }

    public interface CompleterCallback {
        List<String> getResult(CommandSender sender);
    }
}
