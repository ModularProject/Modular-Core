package fr.qcqteam.modular.command;

import fr.qcqteam.modular.command.utils.CommandInfo;
import fr.qcqteam.modular.command.utils.ICommand;
import fr.qcqteam.modular.command.utils.ICommandArg;
import fr.qcqteam.modular.utils.Logger;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Optional;

/**
 * <b>Classe répertoriant tous les argument d'une commande cible.</b>
 *
 * @author HoxiSword
 * @version 1.0.0
 */
public class CommandSpec {

    private final ICommand command;
    private final Object object;
    private HashMap<ICommandArg, Method> arguments = new HashMap<>();
    private Class clazz;
    private boolean enabled;

    /**
     * Constructeur appelé uniquement par TCommandHandler
     * @see CommandHandler#registerCommand(Class, fr.qcqteam.modular.module.IModule)
     *
     * <p>
     *     Récupère tous les arguments de la commande clazz
     * </p>
     *
     * @param clazz Classe annotée de TCommand
     * @see ICommand
     */
    CommandSpec(Class clazz) throws IllegalAccessException, InstantiationException {
        this.clazz = clazz;
        this.command = (ICommand) clazz.getAnnotation(ICommand.class);
        this.object = clazz.newInstance();
        for (Method method : clazz.getDeclaredMethods()){
            if (method.getAnnotation(ICommandArg.class) != null)
                this.arguments.put(method.getAnnotation(ICommandArg.class), method);
        }
        enabled = true;
    }

    public ICommand getCommand() {
        return command;
    }

    public HashMap<ICommandArg, Method> getArguments() {
        return arguments;
    }


    /**
     * Exécute la méthode de l'argument en paramètre avec les informations de commandes
     *
     * @param argument Argument ciblé
     * @param infos Informations de la commande
     * @param <U> U
     * @param <T> T
     *
     * @see CommandInfo
     */
    public <U extends CommandSender, T extends CommandInfo<U>> void invokeArgument(ICommandArg argument, T infos){
        if (!arguments.containsKey(argument))
            return;
        try {
            this.arguments.get(argument).invoke(this.object, infos);
        } catch (IllegalAccessException | InvocationTargetException e) {
            Logger.severe("Can't execute command : " + this.command.commandName());
            e.printStackTrace();
        }
    }

    public Optional<Class<? extends CommandInfo<? extends CommandSender>>> getInfosType(ICommandArg argument){
        if (!arguments.containsKey(argument))
            return Optional.empty();
        try {
            return Optional.of((Class<? extends CommandInfo<? extends CommandSender>>)arguments.get(argument).getParameterTypes()[0]);
        } catch (ClassCastException e) {
            Logger.severe("Can't execute command '" + this.command.commandName() + "' : Argument is not of type CommandInfo.", e);
        }
        return Optional.empty();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Class getCommandClass() {
        return clazz;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
