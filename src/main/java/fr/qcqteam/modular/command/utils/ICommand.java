package fr.qcqteam.modular.command.utils;

import fr.qcqteam.modular.command.CommandHandler;
import fr.qcqteam.modular.module.IModule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <b>Annotation de classe pour créer une commande</b>
 *
 * <i>Important : Tous les arguments ci-dessous sont prioritaires aux doublons sur les TCommandArg</i>
 * <p>
 * Chaque commande doit être enregistrée avec la méthode {@link CommandHandler#registerCommand(Class, IModule)}
 * </p>
 * @author HoxiSword
 * @version 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ICommand {

    /**
     * <b>Nom de la commande</b>
     *
     * @return le nom de la commande
     */
    String commandName();

    /**
     * <b>Aliases de la commande</b>
     *
     * @return tableau des aliases de la commande
     */
    String[] aliases() default {};

    /**
     * Permission à renseigner pour exécuter la commande.
     * <p>
     * Celle-ci est comprise dans l'interval.
     * Dans le cas par défaut, un TPlayer de rang STAFF peut l'exécuter.
     * </p>
     * @return le niveau de permission minimal (&gt;=) pour exécuter la commande.
     * @see String
     */
    String permission() default "";

    /**
     * <p>
     * Usage de la commande, présente dans le /help et indiqué lors d'une erreur.
     * Exemple : /gamemode &lt;creative:survival:spectator:adventure&gt; [pseudo]
     * </p>
     *
     * <p>
     * Convention de nommage des arguments :
     * Argument fixe : &lt;argument&gt; ; Séparation avec des ':'
     * Argument optionnel : [argument]
     * </p>
     *
     * <p>
     * Si la commande est trop complexe, prévoir un /cmd help, à renseigner ici.
     * </p>
     *
     * @return l'usage de la commande
     */
    String usage();

    /**
     * <b>Permissions de plateforme</b>
     *
     * <p>
     * Permission dépendant de la plateforme d'exécution de la commande. (Joueur, Console, CommandBlock)
     * </p>
     *
     * @return Tableau contenant toutes les plateformes autorisées
     * @see CommandSenderType
     */
    CommandSenderType[] commandSenderType() default {CommandSenderType.PLAYER, CommandSenderType.CONSOLE};

    /**
     * <b>Affichage dans le TABCompleter et dans le Help</b>
     *
     * @return boolean : Si faux, la commande n'est pas affichée dans le TAB ou dans le /help
     */
    boolean hidden() default false;
}
