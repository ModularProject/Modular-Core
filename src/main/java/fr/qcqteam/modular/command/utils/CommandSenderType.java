package fr.qcqteam.modular.command.utils;

/**
 * <b>Plateformes d'exécution de commandes</b>
 *
 * @author HoxiSword
 * @version 1.0.0
 */
public enum CommandSenderType {

    PLAYER,
    CONSOLE,
    COMMAND_BLOCK

}
