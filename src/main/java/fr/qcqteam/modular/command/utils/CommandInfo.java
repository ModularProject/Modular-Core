package fr.qcqteam.modular.command.utils;

import org.bukkit.command.CommandSender;

/**
 * <b>Classe contenant les informations d'une commande</b>
 *
 * <p>
 *     Cette classe à deux stades :
 *     - Le commandSender est un joueur
 *     - Le commandSender n'est pas un joueur
 *
 *     Lorsque c'est un joueur, la convertion au TPlayer est automatique.
 * </p>
 *
 * @author HoxiSword
 * @version 1.0.0
 */
public class CommandInfo<T extends CommandSender> {

    private final T commandSender;
    private final String[] arguments;

    public CommandInfo(CommandSender sender, String[] arguments) throws ClassCastException {
        this.commandSender = (T)sender;
        this.arguments = arguments;
    }

    public String[] getArguments() {
        return arguments;
    }

    public T getCommandSender() {
        return commandSender;
    }
}
