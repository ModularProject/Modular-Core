package fr.qcqteam.modular.command.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <b>Annotation permettant d'enregistrer une suite d'arguments</b>
 *
 * <p>
 * A utiliser sur une méthode ayant pour <i>seul argument TCommandInfo</i>.
 * Le nom de la méthode n'a pas d'importance.
 * </p>
 *
 * @author HoxiSword
 * @version 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ICommandArg {

    /**
     * <p>
     * Vide si la commande n'a pas d'argument,
     * Pour utiliser des arguments dynamiques, entourer le nom de l'argument de %
     * Exemple : %valeur%
     * </p>
     *
     * <b>En cas d'alias d'argument, renseigner une nouvelle méthode prenant en compte le nouvel argument
     * et appeler la méthode mère.</b>
     *
     * @return chaque argument dans l'ordre chronologique.
     */
    String[] arguments() default {};

    /**
     * Permission à renseigner pour exécuter la commande.
     * <p>
     * Celle-ci est comprise dans l'interval.
     * Dans le cas par défaut, un TPlayer de rang STAFF peut l'exécuter.
     * </p>
     * @return le niveau de permission minimal (&gt;=) pour exécuter la commande.
     * @see String
     */
    String permission() default "";

    /**
     * <p>
     * Description de la fonction appelée, présente dans le /help
     * </p>
     *
     * @return la description da fonction appelée avec l'argument.
     */
    String description();

    /**
     * <b>Permissions de plateforme</b>
     *
     * <p>
     * Permission dépendant de la plateforme d'exécution de la commande. (Joueur, Console, CommandBlock)
     * </p>
     *
     * @return Tableau contenant toutes les plateformes autorisées
     * @see CommandSenderType
     */
    CommandSenderType[] commandSenderType() default {CommandSenderType.PLAYER, CommandSenderType.CONSOLE};

    /**
     * <b>Completeur de commande (TAB)</b>
     *
     * <i>
     * Si vide, tous les arguments sont complétés avec le BukkitCommandCompleter.NONE.
     * </i>
     *
     * @return Tableau des types de completeurs dans l'ordre des arguments.
     * @see String
     */
    String[] completerKeys() default {};

    /**
     * <b>Affichage dans le TABCompleter et dans le Help</b>
     *
     * @return boolean : Si faux, la commande n'est pas affichée dans le TAB ou dans le /help
     */
    boolean hidden() default false;
}
