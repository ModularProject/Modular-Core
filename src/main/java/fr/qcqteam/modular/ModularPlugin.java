package fr.qcqteam.modular;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by HoxiSword on 08/06/2020 for modular
 */
public class ModularPlugin extends JavaPlugin {

    private final ModularCore modularCore;

    public ModularPlugin() {
        this.modularCore = new ModularCore(this);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        modularCore.onLoad();
    }

    @Override
    public void onEnable() {
        modularCore.onEnable();
    }

    @Override
    public void onDisable() {
        modularCore.onDisable();
    }
}
