package fr.qcqteam.modular.module;

import fr.qcqteam.modular.utils.Logger;
import lombok.Getter;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class ModuleClassLoader extends URLClassLoader {
    private static List<String> blacklistPackageName = Arrays.asList("org.bukkit.", "net.minecraft.");

    static {
        try {
            Method method = ClassLoader.class.getDeclaredMethod("registerAsParallelCapable");
            if (method != null) {
                boolean oldAccessible = method.isAccessible();
                method.setAccessible(true);
                method.invoke(null);
                method.setAccessible(oldAccessible);
                Bukkit.getLogger().log(Level.INFO, "Set ClassLoader as parallel capable");
            }
        } catch (NoSuchMethodException ignored) {
        } catch (Exception var3) {
            Bukkit.getLogger().log(Level.WARNING, "Error setting ClassLoader as parallel capable", var3);
        }

    }

    @Getter
    private Class<? extends IModule> moduleMainClass;
    @Getter
    private File file;
    @Getter
    private ModuleDescription description;

    @SuppressWarnings("unchecked")
    ModuleClassLoader(ModuleLoader loader, ClassLoader parent, ModuleDescription description, File file) throws MalformedURLException {
        super(new URL[]{file.toURI().toURL()}, parent);

        Validate.notNull(loader, "Loader can not be null");

        this.file = file;
        this.description = description;
        this.loader = loader;

        Class mainClass;

        try {
            mainClass = Class.forName(description.getMain(), true, this);
        } catch (ClassNotFoundException e) {
            Logger.severe("Cannot find main class `" + description.getMain() + "`");
            return;
        }

        if (IModule.class.isAssignableFrom(mainClass)) {
            this.moduleMainClass = (Class<? extends IModule>) mainClass;
        } else {
            Logger.severe("Declared main class `" + description.getMain() + "` does not implement from class IModule");
        }
    }

    private ModuleLoader loader;

    private static boolean isClassNameBlacklist(String name) {
        return blacklistPackageName.stream().anyMatch(name::startsWith);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return findClass(name, true);
    }

    Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
        if (!ModuleClassLoader.isClassNameBlacklist(name)) {
            Class<?> clazz = null;
            if (checkGlobal) {
                clazz = this.loader.getClassByName(name);
            }
            return clazz == null ? super.findClass(name) : clazz;
        } else {
            throw new ClassNotFoundException(name);
        }
    }

    public IModule newModuleInstance() {
        try {
            IModule module = moduleMainClass.newInstance();
            module.setDescriptionFile(getDescription());
            return module;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
