package fr.qcqteam.modular.module;

public class MissingDependencyException extends Exception {
    public MissingDependencyException(String name, String dependency) {
        super("Module " + name + " didn't find dependency named " + dependency + ".");
    }
}
