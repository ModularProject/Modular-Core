package fr.qcqteam.modular.module;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.utils.Logger;
import fr.qcqteam.modular.utils.Tuple;
import org.apache.commons.lang.Validate;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ModuleLoader {

    private HashMap<String, ModuleClassLoader> classLoaderMap;
    private HashMap<String, Tuple<ModuleClassLoader, Class<?>>> classes;

    ModuleLoader() {
        this.classLoaderMap = new HashMap<>();
        classes = new HashMap<>();
    }

    private static ModuleDescription getModuleDescriptionFile(File file) throws InvalidDescriptionException {
        Validate.notNull(file, "File cannot be null");
        JarFile jar = null;
        InputStream stream = null;

        ModuleDescription description;

        try {
            jar = new JarFile(file);
            JarEntry entry = jar.getJarEntry("module.yml");

            if (entry == null)
                throw new InvalidDescriptionException(new FileNotFoundException("Jar does not contain module.yml"));

            stream = jar.getInputStream(entry);
            description = new ModuleDescription(stream);

        } catch (IOException | YAMLException e) {
            throw new InvalidDescriptionException(e);
        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException ignored) {
                }
            }
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ignored) {
                }
            }
        }

        return description;
    }

    public static HashMap<File, ModuleDescription> getAllModuleDescriptionFiles() {
        HashMap<File, ModuleDescription> descriptions = new HashMap<>();
        File dir = new File(ModularCore.get().getDataFolder(), "modules");
        if (dir.isDirectory()) {
            for (File file : Objects.requireNonNull(dir.listFiles())) {
                if (file.getName().endsWith(".jar")) {
                    try {
                        ModuleDescription desc = ModuleLoader.getModuleDescriptionFile(file);
                        if (desc != null) {
                            descriptions.put(file, desc);
                        }
                    } catch (InvalidDescriptionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return descriptions;
    }

    IModule loadModule(File file, ModuleDescription description) throws InvalidPluginException {
        Validate.notNull(file, "File cannot be null");
        try {
            if (!file.exists()) {
                throw new FileNotFoundException(file.getName());
            } else {
                ModuleClassLoader classLoader = new ModuleClassLoader(this, this.getClass().getClassLoader(), description, file);
                this.classLoaderMap.put(description.getName(), classLoader);
                IModule module = classLoader.newModuleInstance();
                saveClass(module.getClass(), module);
                return module;
            }
        } catch (Throwable e) {
            throw new InvalidPluginException(e);
        }
    }

    void unloadModule(String module) {
        ModuleClassLoader classLoader = this.classLoaderMap.get(module);
        removeClasses(classLoader);
        try {
            classLoader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.classLoaderMap.remove(module);
    }

    public Class<?> getClassByName(String name) {
        if (this.classes.containsKey(name))
            return this.classes.get(name).getObjectB();
        for (ModuleClassLoader classLoader : this.classLoaderMap.values()) {
            try {
                Class<?> clazz = classLoader.findClass(name, false);
                classes.put(name, new Tuple<>(classLoader, clazz));
                return clazz;
            } catch (ClassNotFoundException ignored) {
            }
        }
        return null;
    }

    public void saveClass(Class<?> name, IModule module) {
        if (this.classes.containsKey(name.getName()))
            return;
        classes.put(name.getName(), new Tuple<>(classLoaderMap.get(module.getDescriptionFile().getName()), name));
    }

    private void removeClasses(ModuleClassLoader loader) {
        List<String> strings = new ArrayList<>();
        this.classes.entrySet().stream().filter(entry -> entry.getValue().getObjectA().equals(loader)).findFirst().ifPresent(entry -> strings.add(entry.getKey()));
        strings.forEach(string -> this.classes.remove(string));
    }

}
