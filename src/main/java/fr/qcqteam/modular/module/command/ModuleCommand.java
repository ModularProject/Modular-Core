package fr.qcqteam.modular.module.command;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.command.completer.CommandCompleter;
import fr.qcqteam.modular.command.utils.CommandInfo;
import fr.qcqteam.modular.command.utils.ICommand;
import fr.qcqteam.modular.command.utils.ICommandArg;
import fr.qcqteam.modular.module.IModule;
import fr.qcqteam.modular.module.MissingDependencyException;
import fr.qcqteam.modular.module.ModuleDescription;
import fr.qcqteam.modular.module.ModuleLoader;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.Map;
import java.util.Optional;

@ICommand(commandName = "module", usage = "/module <load:unload:reload> <moduleId>", permission = "modular.command.module")
public class ModuleCommand {

    private static final String PREFIX = "§7[§3Module§7] §r";

    @ICommandArg(arguments = {"load", "%moduleId%"}, description = "Charge un module déchargé.", completerKeys = {CommandCompleter.ARGUMENT_KEY, CommandCompleter.UNLOADED_MODULES})
    public void load(CommandInfo<CommandSender> info){
        String moduleId = info.getArguments()[1];
        Map<File, ModuleDescription> map = ModuleLoader.getAllModuleDescriptionFiles();

        Map.Entry<File, ModuleDescription> entry = null;
        for (Map.Entry<File, ModuleDescription> descriptionEntry : map.entrySet()) {
            if (descriptionEntry.getValue().getName().equals(moduleId)) {
                entry = descriptionEntry;
                break;
            }
        }

        if (entry == null) {
            info.getCommandSender().sendMessage(PREFIX + "§cLe module '" + moduleId + "' n'existe pas.");
            return;
        }

        if (ModularCore.get().getModuleManager().getModules().containsKey(moduleId)) {
            info.getCommandSender().sendMessage(PREFIX + "§cLe module '" + moduleId + "' est déjà chargé");
            return;
        }

        try {
            info.getCommandSender().sendMessage(PREFIX + "§aChargement du module en cours...");
            ModularCore.get().getModuleManager().loadOneModule(entry.getKey(), entry.getValue());
        } catch (MissingDependencyException e) {
            info.getCommandSender().sendMessage(PREFIX + "§cLe module n'a pas pu être chargé... Une dépendance n'a pas été trouvée.");
            e.printStackTrace();
        } catch (InvalidPluginException e) {
            info.getCommandSender().sendMessage(PREFIX + "§cLe module n'a pas pu être chargé... Une erreur est survenue.");
            e.printStackTrace();
        }
    }

    @ICommandArg(arguments = {"unload", "%moduleId%"}, description = "Décharge un module chargé.", completerKeys = {CommandCompleter.ARGUMENT_KEY, CommandCompleter.LOADED_MODULES})
    public void unload(CommandInfo<CommandSender> info){
        String moduleId = info.getArguments()[1];
        Optional<IModule> module = ModularCore.get().getModuleManager().getModule(moduleId);
        if (!module.isPresent()){
            info.getCommandSender().sendMessage(PREFIX + "§cLe module '" + moduleId + "' n'existe pas.");
            return;
        }

        info.getCommandSender().sendMessage(PREFIX + "§aDéchargement du module en cours...");
        ModularCore.get().getModuleManager().unloadModule(module.get());
        info.getCommandSender().sendMessage(PREFIX + "§aLe module a été déchargé avec succès !");
    }

    @ICommandArg(arguments = {"reload", "%moduleId%"}, description = "Recharge un module chargé ou déchargé", completerKeys = {CommandCompleter.ARGUMENT_KEY, CommandCompleter.LOADED_MODULES})
    public void reload(CommandInfo<CommandSender> info){
        unload(info);
        load(info);
    }

    @ICommandArg(arguments = {"list"}, description = "Liste les modules.", completerKeys = {CommandCompleter.ARGUMENT_KEY})
    public void list(CommandInfo<CommandSender> info){
        info.getCommandSender().sendMessage("§6 ------ §fListe des modules §6------ ");
        info.getCommandSender().sendMessage(" ");
        for (IModule module : ModularCore.get().getModuleManager().getModules().values()) {
            info.getCommandSender().sendMessage(" §7* §a" + module.getDescriptionFile().getName() + " [ Author : " + module.getDescriptionFile().getAuthor() + ", version : " + module.getDescriptionFile().getVersion() + " ]");
        }
        info.getCommandSender().sendMessage(" ");
        info.getCommandSender().sendMessage("§6---------------------------");
    }

    @ICommandArg(arguments = {"info", "%moduleId%"}, description = "Affiche les informations d'un module.", completerKeys = {CommandCompleter.ARGUMENT_KEY, CommandCompleter.LOADED_MODULES})
    public void info(CommandInfo<CommandSender> info){
        String moduleId = info.getArguments()[1];
        Optional<IModule> optionalModule = ModularCore.get().getModuleManager().getModule(moduleId);
        if (!optionalModule.isPresent()){
            info.getCommandSender().sendMessage(PREFIX + "§cLe module '" + moduleId + "' n'existe pas.");
            return;
        }
        IModule module = optionalModule.get();
        info.getCommandSender().sendMessage("§6 ------ §fInfos : " + module.getDescriptionFile().getName() + " §6------ ");
        info.getCommandSender().sendMessage(" ");
        info.getCommandSender().sendMessage(" §7* Author : " + module.getDescriptionFile().getAuthor());
        info.getCommandSender().sendMessage(" §7* Version : " + module.getDescriptionFile().getVersion());
        info.getCommandSender().sendMessage(" §7* Modules Dependencies : ");
        for (String dep : module.getDescriptionFile().getDependencies()){
            info.getCommandSender().sendMessage("   " + (ModularCore.get().getModuleManager().getModule(dep).isPresent() ?"§a":"§c") + "- " + dep);
        }
        info.getCommandSender().sendMessage(" §7* Plugins Dependencies : ");
        for (String dep : module.getDescriptionFile().getBukkitDependencies()){
            Plugin m = Bukkit.getPluginManager().getPlugin(dep);
            if (m == null)
                continue;
            info.getCommandSender().sendMessage("   " + (m.isEnabled()?"§a":"§c") + "- " + dep);
        }
        info.getCommandSender().sendMessage(" ");
        info.getCommandSender().sendMessage("§6---------------------------");
    }

}
