package fr.qcqteam.modular.module;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.command.CommandHandler;
import fr.qcqteam.modular.event.InfoInitEvent;
import fr.qcqteam.modular.event.ModuleLoadEvent;
import fr.qcqteam.modular.event.ModuleUnloadEvent;
import fr.qcqteam.modular.player.PlayerPropertyManager;
import fr.qcqteam.modular.utils.ListenerUtils;
import fr.qcqteam.modular.utils.Logger;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.InvalidPluginException;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ModuleManager {
    /*private static final Comparator<Map.Entry<File, ModuleDescription>> DEPENDENCY_SORTER = (o1, o2) -> {
        if (o1.getValue().getDependencies().contains(o2.getValue().getName()))
            return 1;
        if (o2.getValue().getDependencies().contains(o1.getValue().getName()))
            return -1;
        return 0;
    };
    @Getter
    private ModuleLoader moduleLoader;
    private HashMap<String, IModule> modulesMap;

    public ModuleManager() {
        this.modulesMap = new HashMap<>();
        this.moduleLoader = new ModuleLoader();
    }

    private static void checkDependencies(ModuleDescription checkedDesc, Collection<ModuleDescription> modulesDescList, List<String> checkedModuleNames) throws MissingDependencyException {
        for (String dependency : checkedDesc.getDependencies()) {
            if (!checkedModuleNames.contains(dependency)) {
                Optional<ModuleDescription> optional = modulesDescList.stream().filter((desc) -> desc.getName().equals(dependency)).findFirst();
                if (!optional.isPresent()) {
                    throw new MissingDependencyException(checkedDesc.getName(), dependency);
                }

                ModuleDescription description = optional.get();
                if (description.getBukkitDependencies() != null) {
                    for (String bukkitDependency : description.getBukkitDependencies()) {
                        if (Bukkit.getPluginManager().getPlugin(bukkitDependency) == null) {
                            throw new MissingDependencyException(checkedDesc.getName(), bukkitDependency);
                        }
                    }
                }

                checkDependencies(description, modulesDescList, checkedModuleNames);
                checkedModuleNames.add(description.getName());
            }
        }
    }

    private boolean loadModule(File file, ModuleDescription module, HashMap<String, ModuleDescription> map, ArrayList<String> checkedModuleName){
        if (module == null)
            return false;

        if (modulesMap.containsKey(module.getName()))
            return true;
        if (checkedModuleName.contains(module.getName())){
            Logger.severe("Circular dependency detected : " + module.getName());
            checkedModuleName.remove(module.getName());
            return false;
        }


        checkedModuleName.add(module.getName());

        Logger.info(module.getDependencies().size() + " dependencies.");

        if (!checkDependency(file, module, map, checkedModuleName)) {
            Logger.severe("Can't load modules dependencies of " + module.getName());
            checkedModuleName.remove(module.getName());
            return false;
        }

        if (module.getBukkitDependencies() != null)
            for (String depend : module.getBukkitDependencies()){
                if (Bukkit.getPluginManager().getPlugin(depend) == null){
                    Logger.severe("An error occurred while loading module '" + module.getName() + "' : Bukkit dependencies '" + depend + "' isn't loaded.");
                    checkedModuleName.remove(module.getName());
                    return false;
                }
            }

        try {
            Logger.info("Loading module '" + module.getName() + "'");
            IModule iModule = this.moduleLoader.loadModule(file, module);
            this.modulesMap.put(module.getName(), iModule);
            Logger.info("Enabling module '" + module.getName() + "'");
            iModule.onEnable();
        } catch (Exception ex){
            Logger.severe("Can't load module " + module.getName() + " : ");
            ex.printStackTrace();
            checkedModuleName.remove(module.getName());
            return false;
        }
        checkedModuleName.remove(module.getName());
        return true;
    }

    private boolean checkDependency(File file, ModuleDescription module, HashMap<String, ModuleDescription> map, ArrayList<String> checkedModuleName){
        for (String depend : module.getDependencies()){
            ModuleDescription description = map.get(depend);
            if (!loadModule(file, description, map, checkedModuleName))
                return false;
        }
        return true;
    }

    public void loadModules() {
        // STEP 1 LOAD DESC FILES
        Map<File, ModuleDescription> descriptions = ModuleLoader.getAllModuleDescriptionFiles();

        // STEP 2 CHECK DEPENDENCIES
        ArrayList<String> checkedModuleName = new ArrayList<>();
        HashMap<String, ModuleDescription> descriptionHashMap = new HashMap<>();

        for (Map.Entry<File, ModuleDescription> entry : new HashSet<>(descriptions.entrySet())) {
            descriptionHashMap.put(entry.getValue().getName(), entry.getValue());
        }

        // STEP 3 LOAD MODULES
        List<Map.Entry<File, ModuleDescription>> descList = new ArrayList<>(descriptions.entrySet());
        for (Map.Entry<File, ModuleDescription> entry : descList) {
            loadModule(entry.getKey(), entry.getValue(), descriptionHashMap, checkedModuleName);
        }
    }

    public void loadOneModule(File file, ModuleDescription description) throws MissingDependencyException, InvalidPluginException {
        List<ModuleDescription> list = this.getModules().values()
                .stream()
                .map(IModule::getDescriptionFile)
                .collect(Collectors.toList());
        List<String> checkedModules = new ArrayList<>();
    }

    public void enableModules() {
        this.modulesMap.forEach((s, iModule) -> {
            try {
                Logger.info("Enabling module '" + iModule.getDescriptionFile().getName() + "'");
                iModule.onEnable();
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    public void disableModules() {
        this.modulesMap.forEach((s, iModule) -> {
            Logger.info("Disabling module '" + iModule.getDescriptionFile().getName() + "'");
            iModule.onDisable();
        });
        List<IModule> modules = new ArrayList<>(this.modulesMap.values()); //to avoid concurrent modification
        modules.forEach((iModule) -> {
            Logger.info("Unloading module '" + iModule.getDescriptionFile().getName() + "'");
            unloadModule(iModule);
        });
    }

    public void unloadModule(IModule module) {
        //TODO work on cleaner way to handle dependent module
        for (IModule depend : getDependentModule(module)) {
            unloadModule(depend);
        }
        if (this.modulesMap.containsKey(module.getDescriptionFile().getName())) {
            module.onDisable();
            PlayerPropertyManager.get().getPropertyTags(module.getDescriptionFile().getName()).
                    forEach(property -> ModularCore.get().getDataManager().ifPresent(manager -> manager.getSaveTags().
                            forEach(save -> manager.getSave(save).values().
                                    forEach(player -> player.removeProperty(property)))));
            CommandHandler.unregisterModuleCommands(module);
            ListenerUtils.unregisterListeners(module);
            moduleLoader.unloadModule(module.getDescriptionFile().getName());
            modulesMap.remove(module.getDescriptionFile().getName());
        }
    }

    private List<IModule> getDependentModule(IModule module) {
        return modulesMap.values()
                .stream()
                .filter(checkedModule -> (checkedModule != null
                        && checkedModule.getDescriptionFile().getDependencies().contains(module.getDescriptionFile().getName())))
                .collect(Collectors.toList());
    }

    public Optional<IModule> getModule(String moduleKey) {
        return Optional.ofNullable(this.modulesMap.getOrDefault(moduleKey, null));
    }

    public HashMap<String, IModule> getModules() {
        return this.modulesMap;
    }*/

    private static final Comparator<Map.Entry<File, ModuleDescription>> DEPENDENCY_SORTER = (o1, o2) -> {
        if (o1.getValue().getDependencies().contains(o2.getValue().getName()))
            return 1;
        if (o2.getValue().getDependencies().contains(o1.getValue().getName()))
            return -1;
        return 0;
    };
    @Getter
    private ModuleLoader moduleLoader;
    private HashMap<String, IModule> modulesMap;

    public ModuleManager() {
        this.modulesMap = new HashMap<>();
        this.moduleLoader = new ModuleLoader();
    }

    private static void checkDependencies(ModuleDescription checkedDesc, Collection<ModuleDescription> modulesDescList, List<String> checkedModuleNames) throws MissingDependencyException {
        for (String dependency : checkedDesc.getDependencies()) {
            if (!checkedModuleNames.contains(dependency)) {
                Optional<ModuleDescription> optional = modulesDescList.stream().filter((desc) -> desc.getName().equals(dependency)).findFirst();
                if (!optional.isPresent()) {
                    throw new MissingDependencyException(checkedDesc.getName(), dependency);
                }

                ModuleDescription description = optional.get();
                if (description.getBukkitDependencies() != null) {
                    for (String bukkitDependency : description.getBukkitDependencies()) {
                        if (Bukkit.getPluginManager().getPlugin(bukkitDependency) == null) {
                            throw new MissingDependencyException(checkedDesc.getName(), bukkitDependency);
                        }
                    }
                }

                checkDependencies(description, modulesDescList, checkedModuleNames);
                checkedModuleNames.add(description.getName());
            }
        }
    }

    private ArrayList<Map.Entry<File, ModuleDescription>> sort(Map<File, ModuleDescription> list){
        ArrayList<Map.Entry<File, ModuleDescription>> descList = new ArrayList<>();
        if (list.isEmpty())
            return descList;

        for (Map.Entry<File, ModuleDescription> entry : list.entrySet()) {
            for (int i = 0; i < descList.size(); i++){
                if (descList.get(i).getValue().getDependencies().contains(entry.getValue().getName())){
                    descList.add(i, entry);
                    break;
                }
            }
            if (descList.contains(entry))
                continue;
            descList.add(entry);
        }

        return descList;
    }

    /**
     * Load all modules
     */
    public void loadModules() {
        // STEP 1 LOAD DESC FILES
        Map<File, ModuleDescription> descriptions = ModuleLoader.getAllModuleDescriptionFiles();

        // STEP 2 CHECK DEPENDENCIES
        ArrayList<String> checkedModuleName = new ArrayList<>();
        for (Map.Entry<File, ModuleDescription> entry : new HashSet<>(descriptions.entrySet())) {
            try {
                if (!checkedModuleName.contains(entry.getValue().getName())) {
                    ModuleManager.checkDependencies(entry.getValue(), descriptions.values(), checkedModuleName);
                }
            } catch (MissingDependencyException e) {
                e.printStackTrace();
                descriptions.remove(entry.getKey());
            }
        }

        // STEP 3 LOAD MODULES
        for (Map.Entry<File, ModuleDescription> entry : sort(descriptions)) {
            try {
                Logger.info("Loading module '" + entry.getValue().getName() + "'");
                IModule module = this.moduleLoader.loadModule(entry.getKey(), entry.getValue());
                Bukkit.getServer().getPluginManager().callEvent(new ModuleLoadEvent(module));
                this.modulesMap.put(entry.getValue().getName(), module);
                Logger.info("Enabling module '" + module.getDescriptionFile().getName() + "'");
                module.onEnable();
            } catch (InvalidPluginException e) {
                e.printStackTrace();
//                Bukkit.getServer().shutdown();
            }
        }
    }

    public void loadOneModule(File file, ModuleDescription description) throws MissingDependencyException, InvalidPluginException {
        List<ModuleDescription> list = this.getModules().values()
                .stream()
                .map(IModule::getDescriptionFile)
                .collect(Collectors.toList());
        List<String> checkedModules = new ArrayList<>();
        ModuleManager.checkDependencies(description, list, checkedModules);

        Logger.info("Loading module '" + description.getName() + "'");
        IModule module = this.moduleLoader.loadModule(file, description);
        Bukkit.getServer().getPluginManager().callEvent(new ModuleLoadEvent(module));
        this.modulesMap.put(description.getName(), module);
        module.onEnable();
    }

    /**
     * Enable all modules
     */
    public void enableModules() {
        this.modulesMap.forEach((s, iModule) -> {
            try {
                Logger.info("Enabling module '" + iModule.getDescriptionFile().getName() + "'");
                iModule.onEnable();
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    /**
     * Disable all modules
     */
    public void disableModules() {
        this.modulesMap.forEach((s, iModule) -> {
            Logger.info("Disabling module '" + iModule.getDescriptionFile().getName() + "'");
            iModule.onDisable();
        });
        List<IModule> modules = new ArrayList<>(this.modulesMap.values()); //to avoid concurrent modification
        modules.forEach((iModule) -> {
            Logger.info("Unloading module '" + iModule.getDescriptionFile().getName() + "'");
            unloadModule(iModule);
        });
    }

    public void unloadModule(IModule module) {
        //TODO work on cleaner way to handle dependent module
        for (IModule depend : getDependentModule(module)) {
            unloadModule(depend);
        }
        if (this.modulesMap.containsKey(module.getDescriptionFile().getName())) {
            module.onDisable();
            Bukkit.getServer().getPluginManager().callEvent(new ModuleUnloadEvent(module));
            PlayerPropertyManager.get().getPropertyTags(module.getDescriptionFile().getName()).
                    forEach(property -> ModularCore.get().getDataManager().ifPresent(manager -> manager.getSaveTags().
                            forEach(save -> manager.getSave(save).values().
                                    forEach(player -> player.removeProperty(property)))));
            CommandHandler.unregisterModuleCommands(module);
            ListenerUtils.unregisterListeners(module);
            moduleLoader.unloadModule(module.getDescriptionFile().getName());
            modulesMap.remove(module.getDescriptionFile().getName());
        }
    }

    private List<IModule> getDependentModule(IModule module) {
        return modulesMap.values()
                .stream()
                .filter(checkedModule -> (checkedModule != null
                        && checkedModule.getDescriptionFile().getDependencies().contains(module.getDescriptionFile().getName())))
                .collect(Collectors.toList());
    }

    public Optional<IModule> getModule(String moduleKey) {
        return Optional.ofNullable(this.modulesMap.getOrDefault(moduleKey, null));
    }

    public HashMap<String, IModule> getModules() {
        return this.modulesMap;
    }
}
