package fr.qcqteam.modular.module;

import fr.qcqteam.modular.ModularCore;
import lombok.Getter;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.PluginAwareness;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ModuleDescription {

    private static final String REGEXP_ARRAY = "\\[\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\")?)?)?)?\\]";

    private static final ThreadLocal<Yaml> YAML = new ThreadLocal<Yaml>() {
        protected Yaml initialValue() {
            return new Yaml(new SafeConstructor() {
                {
                    this.yamlConstructors.put(null, new AbstractConstruct() {
                        public Object construct(final Node node) {
                            return !node.getTag().startsWith("!@") ? SafeConstructor.undefinedConstructor.construct(node) : new PluginAwareness() {
                                public String toString() {
                                    return node.toString();
                                }
                            };
                        }
                    });
                    PluginAwareness.Flags[] var2;
                    int var3 = (var2 = PluginAwareness.Flags.values()).length;

                    for (int var4 = 0; var4 < var3; ++var4) {
                        final PluginAwareness.Flags flag = var2[var4];
                        this.yamlConstructors.put(new Tag("!@" + flag.name()), new AbstractConstruct() {
                            public PluginAwareness.Flags construct(Node node) {
                                return flag;
                            }
                        });
                    }
                }
            });
        }
    };

    @Getter
    private String main;
    @Getter
    private String author;
    @Getter
    private String name;
    @Getter
    private String version;
    @Getter
    private List<String> dependencies;
    @Getter
    private List<String> bukkitDependencies;

    public ModuleDescription(InputStream stream) throws InvalidDescriptionException {
        this.loadMap(this.asMap(YAML.get().load(stream)));
    }

    public ModuleDescription(ModularCore plugin) {
        main = "";
        author = plugin.getPlugin().getDescription().getAuthors().get(0);
        name = plugin.getPlugin().getDescription().getName();
        version = plugin.getPlugin().getDescription().getVersion();
        dependencies = new ArrayList<>();
        bukkitDependencies = new ArrayList<>();
    }

    private static List<String> formatArray(String string) {
        string = string.replace(" ", "");
        string = string.replace("[", "").replace("]", "");
        List<String> depArray = Arrays.asList(string.split(","));
        int i;
        while ((i = depArray.indexOf("")) != -1) {
            depArray.remove(i);
        }
        return depArray;
    }

    private void loadMap(Map<?, ?> map) throws InvalidDescriptionException {

        try {
            this.main = map.get("main").toString();
        } catch (NullPointerException e) {
            throw new InvalidDescriptionException("main is not defined");
        } catch (ClassCastException var18) {
            throw new InvalidDescriptionException(var18, "main is of wrong type");
        }

        try {
            this.version = map.get("version").toString();
        } catch (NullPointerException e) {
            throw new InvalidDescriptionException("version is not defined");
        } catch (ClassCastException var18) {
            throw new InvalidDescriptionException(var18, "version is of wrong type");
        }

        try {
            this.author = map.get("author").toString();
        } catch (NullPointerException e) {
            throw new InvalidDescriptionException("author is not defined");
        } catch (ClassCastException var18) {
            throw new InvalidDescriptionException(var18, "author is of wrong type");
        }

        try {
            this.name = map.get("name").toString();
        } catch (NullPointerException e) {
            throw new InvalidDescriptionException("name is not defined");
        } catch (ClassCastException var18) {
            throw new InvalidDescriptionException(var18, "name is of wrong type");
        }

        try {
            this.dependencies = formatArray(map.get("depends").toString());
        } catch (NullPointerException e) {
            this.dependencies = new ArrayList<>();
        } catch (ClassCastException e) {
            throw new InvalidDescriptionException(e, "dependencies are of wrong type");
        }

        try {
            this.bukkitDependencies = formatArray(map.get("bukkitDepends").toString());
        } catch (NullPointerException e) {
            this.bukkitDependencies = new ArrayList<>();
        } catch (ClassCastException e) {
            throw new InvalidDescriptionException(e, "BukkitDependencies are of wrong type");
        }
    }

    private Map<?, ?> asMap(Object object) throws InvalidDescriptionException {
        if (object instanceof Map) {
            return (Map) object;
        } else {
            throw new InvalidDescriptionException(object + " is not properly structured.");
        }
    }
}
