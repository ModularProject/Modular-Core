package fr.qcqteam.modular.module;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 27/03/2018 for modularworkspace.
 */
public abstract class IModule {

    @Getter
    @Setter(value = AccessLevel.PACKAGE)
    private ModuleDescription descriptionFile;

    /**
     * Called during bukkit enabling
     */
    public void onEnable(){

    }

    /**
     * Called during bukkit disabling
     */
    public void onDisable(){

    }
}
