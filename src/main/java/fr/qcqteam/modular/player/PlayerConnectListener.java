package fr.qcqteam.modular.player;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.utils.Logger;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;

public class PlayerConnectListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        ModularCore.get().getDataManager().ifPresent( dataManager -> {
            DefaultPlayerManager manager = (DefaultPlayerManager)dataManager;
            PlayerInfo pInfo = manager.addPlayerInfo(event.getPlayer());
            manager.initPlayerInfo(pInfo);
            manager.loadPlayerInfo(pInfo);
        });
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Logger.info("Leave");
        ModularCore.get().getDataManager().ifPresent( dataManager -> {
            Logger.info("ManagerPresent");
            DefaultPlayerManager manager = (DefaultPlayerManager)dataManager;
            PlayerInfo pInfo = manager.getPlayerInfo(event.getPlayer().getUniqueId());
            manager.savePlayerInfo(pInfo);
            manager.removePlayerInfo(event.getPlayer());
        });

    }
}
