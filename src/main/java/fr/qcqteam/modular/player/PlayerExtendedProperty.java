package fr.qcqteam.modular.player;

import fr.qcqteam.modular.info.property.IExtendedProperty;
import lombok.AccessLevel;
import lombok.Getter;

/**
 * Created by Theophile on 2018-12-20 for modularworkspace.
 */
@SuppressWarnings("WeakerAccess")
public abstract class PlayerExtendedProperty<T extends PlayerInfo> extends IExtendedProperty {

    @Getter(AccessLevel.PROTECTED)
    private T player;

    PlayerExtendedProperty<T> setInfo(T t){
        this.player = t;
        return this;
    }

}
