package fr.qcqteam.modular.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * Created by HoxiSword on 05/10/2019 for modular
 */
public class DefaultPlayerManager extends AbstractPlayerManager<PlayerInfo, PlayerPropertyManager> {

    public DefaultPlayerManager() {
        super(new PlayerConnectListener(), PlayerPropertyManager.class);
    }

    /**
     * Get PlayerInfo Object of the default save
     *
     * @param uuid bukkit player uuid
     * @return playerinfo if found, null if not
     */
    @SuppressWarnings("WeakerAccess")
    public PlayerInfo getPlayerInfo(UUID uuid) {
        return getPlayerInfo(DEFAULT_SAVE, uuid.toString());
    }

    /**
     * Get PlayerInfo Object of a specific save
     *
     * @param saveTag tag of the save
     * @param player  bukkit object player
     * @return playerinfo if found, null if not
     */
    @SuppressWarnings("WeakerAccess")
    public PlayerInfo getPlayerInfo(String saveTag, Player player) {
        return getPlayerInfo(saveTag, player.getUniqueId().toString());
    }

    /**
     * Add a PlayerInfo in a specific save
     *
     * @param saveTag tag of the save
     * @param player  bukkit player object
     * @return PlayerInfo created
     */
    @SuppressWarnings("WeakerAccess")
    public PlayerInfo addPlayerInfo(String saveTag, Player player) {
        PlayerInfo info = new PlayerInfo(player.getUniqueId());
        addPlayerInfo(saveTag, info);
        return info;
    }

    public PlayerInfo addPlayerInfo(Player player){
        return addPlayerInfo(DEFAULT_SAVE, player);
    }

    /**
     * Remove a PlayerInfo from a save.
     *
     * @param saveTag tag of the save
     * @param player  bukkit player object
     * @return PlayerInfo removed
     */
    @SuppressWarnings("WeakerAccess")
    public void removePlayerInfo(String saveTag, Player player) {
        removePlayerInfo(saveTag, player.getUniqueId().toString());
    }

    public void removePlayerInfo(Player player){
        removePlayerInfo(DEFAULT_SAVE, player);
    }

    public void savePlayerInfo(UUID uuid) {
        savePlayerInfo(getPlayerInfo(uuid));
    }

    @Override
    public void savePlayerInfos() {
        Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> savePlayerInfo(player.getUniqueId()));
    }
}
