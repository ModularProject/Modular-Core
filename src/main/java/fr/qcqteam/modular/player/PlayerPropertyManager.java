package fr.qcqteam.modular.player;

import fr.qcqteam.modular.info.property.PropertyManager;

/**
 * Created by Theophile on 2018-12-20 for modularworkspace.
 */
public class PlayerPropertyManager extends PropertyManager<PlayerExtendedProperty<PlayerInfo>> {

    private static PlayerPropertyManager instance = null;

    public static PlayerPropertyManager get() {
        if (instance == null)
            instance = new PlayerPropertyManager();
        return instance;
    }

}
