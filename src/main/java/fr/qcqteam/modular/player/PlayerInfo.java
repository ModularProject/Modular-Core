package fr.qcqteam.modular.player;

import fr.qcqteam.modular.info.Info;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Theophile on 2018-12-20 for modularworkspace.
 */
public class PlayerInfo extends Info<PlayerExtendedProperty<? extends PlayerInfo>> {

    @Getter
    protected UUID uuid;

    protected PlayerInfo(UUID uuid) {
        super(uuid.toString());
        this.uuid = uuid;
    }

    protected PlayerInfo(String key, UUID uuid){
        super(key);
        this.uuid = uuid;
    }

    public Player getPlayer(){
        return Bukkit.getPlayer(this.uuid);
    }

}
