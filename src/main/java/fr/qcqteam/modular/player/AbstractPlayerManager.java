package fr.qcqteam.modular.player;

import fr.qcqteam.modular.ModularCore;
import fr.qcqteam.modular.database.DAONotFoundException;
import fr.qcqteam.modular.database.DAOParameter;
import fr.qcqteam.modular.event.InfoInitEvent;
import fr.qcqteam.modular.event.InfoLoadEvent;
import fr.qcqteam.modular.event.InfoSaveEvent;
import fr.qcqteam.modular.info.Info;
import fr.qcqteam.modular.info.property.IExtendedProperty;
import fr.qcqteam.modular.info.property.PropertyManager;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractPlayerManager<T extends PlayerInfo, U extends PropertyManager<PlayerExtendedProperty<T>>> {

    protected static final String DEFAULT_SAVE = "DEFAULT";
    @Getter
    private final Listener connectionListener;
    private final Class<U> propertyManagerClass;
    protected HashMap<String, HashMap<String, T>> savePlayerInfoMap = new HashMap<>();
    private DAOParameter parameter;

    public AbstractPlayerManager(@NonNull Listener connectionListener, Class<U> propertyManagerClass) {
        savePlayerInfoMap.put(DEFAULT_SAVE, new HashMap<>());
        this.connectionListener = connectionListener;
        this.propertyManagerClass = propertyManagerClass;
    }

    public Collection<String> getSaveTags() {
        return savePlayerInfoMap.keySet();
    }

    public void initParameter() {
        try {
            this.parameter = ModularCore.get().getDaoFactory().getDAO(Info.class).getDefaultParameter();
        } catch (DAONotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public DAOParameter getParameter() {
        return parameter;
    }

    public HashMap<String, T> getPlayersMap(String save) {
        return savePlayerInfoMap.get(save);
    }

    public Collection<T> getPlayers() {
        return savePlayerInfoMap.get(DEFAULT_SAVE).values();
    }

    /**
     * Get all data from a save
     *
     * @param saveTag tag of the save
     * @return data of a specific save
     */
    @SuppressWarnings("WeakerAccess")
    public HashMap<String, T> getSave(String saveTag) {
        return savePlayerInfoMap.get(saveTag);
    }

    /**
     * Get all data from the default save
     *
     * @return data of the default save
     */
    @SuppressWarnings("WeakerAccess")
    public HashMap<String, T> getDefaultSave() {
        return getSave(AbstractPlayerManager.DEFAULT_SAVE);
    }

    /**
     * Add a save for all user. Must be call before first connection of any player.
     *
     * @param saveTag tag of the save. Must be unique
     */
    public void addSave(String saveTag) {
        savePlayerInfoMap.put(saveTag, new HashMap<>());

        for (Map.Entry<String, T> entry : this.getDefaultSave().entrySet()) {
            this.addPlayerInfo(saveTag, entry.getValue());
        }
    }

    /**
     * Remove a save for all user. Delete all data of this save. Use it carefully.
     *
     * @param saveTag tag of the save
     */
    public void removeSave(String saveTag) {
        savePlayerInfoMap.remove(saveTag);
    }


    /**
     * Get PlayerInfo Object of a specific save
     *
     * @param saveTag tag of the save
     * @param key     uuid of the user
     * @return playerinfo if found, null if not
     */
    @SuppressWarnings("WeakerAccess")
    public T getPlayerInfo(String saveTag, String key) {
        return savePlayerInfoMap.get(saveTag).get(key);
    }

    /**
     * Get PlayerInfo Object of the default save
     *
     * @param key bukkit object player
     * @return playerinfo if found, null if not
     */
    public T getPlayerInfo(String key) {
        return getPlayerInfo(AbstractPlayerManager.DEFAULT_SAVE, key);
    }


    /**
     * Add a PlayerInfo in a specific save
     *
     * @param saveTag tag of the save
     * @param player  uuid of the player
     * @return PlayerInfo created
     */
    @SuppressWarnings("WeakerAccess")
    public void addPlayerInfo(String saveTag, T player) {
        savePlayerInfoMap.get(saveTag).put(player.getId(), player);
    }

    /**
     * Add a PlayerInfo in the default save.
     *
     * @param player bukkit player object
     * @return PlayerInfo created
     */
    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    public void addPlayerInfo(T player) {
        addPlayerInfo(AbstractPlayerManager.DEFAULT_SAVE, player);
    }


    /**
     * Remove a PlayerInfo from a save.
     *
     * @param saveTag tag of the save
     * @param key     uuid of the player
     * @return PlayerInfo removed
     */
    @SuppressWarnings("WeakerAccess")
    public void removePlayerInfo(String saveTag, String key) {
        savePlayerInfoMap.get(saveTag).remove(key);
    }

    /**
     * Remove a PlayerInfo from the default save.
     *
     * @param key bukkit player object
     * @return playerinfo removed
     */
    @SuppressWarnings({"unused", "UnusedReturnValue", "WeakerAccess"})
    public void removePlayerInfo(String key) {
        removePlayerInfo(AbstractPlayerManager.DEFAULT_SAVE, key);
    }

    @SuppressWarnings("WeakerAccess")
    public void initPlayerInfo(T playerInfo) {
        try {
            Bukkit.getServer().getPluginManager().callEvent(new InfoInitEvent.Pre(playerInfo));
            Method method = propertyManagerClass.getMethod("get");
            U obj = (U) method.invoke(null);

            obj.getAllPropertiesTag().forEach((module, tags) -> tags.forEach(tag -> {
                try {
                    playerInfo.addProperty(tag, obj.getPropertyClass(module, tag).newInstance().setInfo(playerInfo));
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }));

            playerInfo.getProperties().forEach(IExtendedProperty::init);
            Bukkit.getServer().getPluginManager().callEvent(new InfoInitEvent.Post(playerInfo));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void loadPlayerInfo(T playerInfo) {
        InfoLoadEvent.Pre pre = new InfoLoadEvent.Pre(playerInfo);
        Bukkit.getServer().getPluginManager().callEvent(pre);
        if (!pre.isCancelled()) {
            try {
                ModularCore.get().getDaoFactory().getDAO(Info.class).load(playerInfo, parameter);
                Bukkit.getServer().getPluginManager().callEvent(new InfoLoadEvent.Post(playerInfo));
            } catch (DAONotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }

    @SuppressWarnings("WeakerAccess")
    public void savePlayerInfo(T playerInfo) {
        InfoSaveEvent.Pre pre = new InfoSaveEvent.Pre(playerInfo);
        Bukkit.getServer().getPluginManager().callEvent(pre);
        if (!pre.isCancelled()) {
            try {
                ModularCore.get().getDaoFactory().getDAO(Info.class).save(playerInfo, parameter);
                Bukkit.getServer().getPluginManager().callEvent(new InfoSaveEvent.Post(playerInfo));
            } catch (DAONotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void savePlayerInfos();

    public void registerConnectListener(Plugin plugin) {
        Bukkit.getServer().getPluginManager().registerEvents(this.connectionListener, plugin);
    }
}
