package fr.qcqteam.modular.info.property;

import fr.qcqteam.modular.utils.Logger;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Theophile on 01/11/2018 for webapi.
 */
public abstract class PropertyManager<U extends IExtendedProperty> {

    private HashMap<String, HashMap<String, Class<? extends U>>> propertiesMap = new HashMap<>();

    public void registerProperty(String module, String propertyTag, Class<? extends U> propertyClass) {
        if (!propertiesMap.containsKey(module))
            propertiesMap.put(module, new HashMap<>());
        propertiesMap.get(module).put(propertyTag, propertyClass);
    }

    public void unregisterProperty(String module, String propertyTag) {
        if (!propertiesMap.containsKey(module))
            return;
        propertiesMap.get(module).remove(propertyTag);
    }

    public void unregisterProperties(String module){
        propertiesMap.remove(module);
    }

    public void flushPropertiesMap() {
        propertiesMap.clear();
    }

    public Class<? extends U> getPropertyClass(String module, String propertyTag) {
        if (!propertiesMap.containsKey(module))
            return null;
        return propertiesMap.get(module).get(propertyTag);
    }

    public Collection<String> getPropertyTags(String module){
        if (!propertiesMap.containsKey(module))
            return new ArrayList<>();
        return this.propertiesMap.get(module).keySet();
    }

    public String getPropertyTag(Class<? extends U> propertyClass) {
        try {
            return propertyClass.newInstance().getPropertyTag();
        } catch (InstantiationException | IllegalAccessException e) {
            Logger.severe(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    public HashMap<String, Collection<String>> getAllPropertiesTag() {
        HashMap<String, Collection<String>> map = new HashMap<>();
        for (String module : propertiesMap.keySet()){
            map.put(module, propertiesMap.get(module).keySet());
        }
        return map;
    }
}
