package fr.qcqteam.modular.info.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SavedItem {
    /**
     * Explicit field name to save
     *
     * @return name of the field
     */
    Class firstGenericClass() default Integer.class;

    Class secondGenericClass() default Integer.class;
}
