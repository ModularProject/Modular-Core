package fr.qcqteam.modular.info.property;

import lombok.NoArgsConstructor;

/**
 * Created by Theophile on 02/08/2018 for webapi.
 */
@NoArgsConstructor
public abstract class IExtendedProperty {

    /**
     * Called before field update
     */
    public void init() {
    }

    /**
     * Called before field load
     */
    abstract public void preLoad();

    /**
     * Called after field load
     */
    abstract public void postLoad();

    /**
     * Called before field save
     */
    abstract public void preSave();

    /**
     * Called after field save
     */
    abstract public void postSave();

    /**
     * Called when field is created
     */
    abstract public void setDefault();

    abstract public String getPropertyTag();
}
