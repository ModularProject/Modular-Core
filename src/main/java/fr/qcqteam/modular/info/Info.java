/*
 * Copyright 404Team (c) 2018. For all uses ask 404Team for approval before.
 */
package fr.qcqteam.modular.info;

import fr.qcqteam.modular.info.property.IExtendedProperty;
import fr.qcqteam.modular.utils.Logger;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Helliot on 19/08/2018 for webapi.
 * Remade by Qilat on 20/09/2019 for modular.
 */
public abstract class Info<T extends IExtendedProperty> {

    @Getter
    private HashMap<String, T> propertiesMap = new HashMap<>();

    @Getter
    private String id;

    protected Info(String id) {
        this.id = id;
    }

    /**
     * Add a property to this User
     *
     * @param property property to add
     */
    public void addProperty(T property){
        this.addProperty(property.getPropertyTag(), property);
    }

    public void addProperty(String propertyTag, T property) {
        if (this.propertiesMap.containsKey(propertyTag)) {
            Logger.warning("Unable to register two properties with the same property tag : " + propertyTag);
        } else {
            this.getPropertiesMap().put(propertyTag, property);
        }
    }

    /**
     * Remove a property to this Info
     * @param propertyTag propertyTag of the property to remove
     * @return Info object
     */
    public Info<T> removeProperty(String propertyTag) {
        if (getPropertiesMap().get(propertyTag) == null) {
            Logger.warning("Unable to remove a property which does not exist in player's data map.");
        } else {
            getPropertiesMap().remove(propertyTag);
        }
        return this;
    }

    public T getProperty(String propertyTag) {
        return this.getPropertiesMap().get(propertyTag);
    }

    public Collection<T> getProperties() {
        return this.propertiesMap.values();
    }
}
